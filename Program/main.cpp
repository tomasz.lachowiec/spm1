#include "mainwindow.h"
#include <QApplication>

#include "Framework.h"

Framework* g_framework = NULL;

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    g_framework = new Framework();

    g_framework->createMainWindow( QApplication::desktop());

    return a.exec();
}
