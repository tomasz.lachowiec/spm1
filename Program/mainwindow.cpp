#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QFileDialog>
#include <QDebug>

#include "inc/global.h"

#include "GUI/Pages/StartPage.h"
#include "GUI/Pages/Modules/ConfigMD04Page.h"

#include "Config/ConfigSoftware.h"
#include "GUI/SoftwareSetup.h"
#include "GUI/Views/DeviceSelectDialog.h"


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
     ui->setupUi(this);
     //ui->calibrationButton->setEnabled(1);
     ui->testDeviceButton->setEnabled(1);
  //    m_settings = new QSettings(g_framework->getSettingsFilePath(), QSettings::IniFormat, this);
    m_leftToolbarButtonsList = ui->leftToolbarFrame->findChildren<QToolButton *>();

    // (tylko przyciski, kt. zmieniaja sie przy wlaczaniu/wylaczaniu sesji)
    m_menuButtonName2viewIdx.insert("configurationButton", VIEW_CONFIGURATION_AP01);
    m_menuButtonName2viewIdx.insert("calibrationButton",  VIEW_CALIBRATION_AP01);


    m_menuItemEnableStyle=".toolButton{color:#FCFCFC;background-color: transparent;border:none;}"
                        ".toolButton:hover{background-color:#1C91D1;border:none;border-radius:4px;color:white;}";
    m_menuItemSelectedStyle = ".toolButton{background-color:rgb(17,158,90);border:none;border-radius:4px;color:white;}";
    m_menuItemDisableStyle = ".toolButton{color:gray;background-color: transparent;border:none;}";



    //zarzadzanie aktywnoscia przyciskow



    //init consoli

    ui->consoleFrame->show();
    m_console = new ConsoleWidget();
    connect(m_console, SIGNAL(closeButtonClicked()), ui->consoleButton, SLOT(toggle()));
    ui->consoleFrame->layout()->addWidget( m_console );

    //ui->menuBar->show();
     //    ui->menuFile->menuAction()->setVisible(true);
     ui->menuBar->setNativeMenuBar(false);  //aby widoczny było menu w programie

     ui->actionConnect->setEnabled(false);

     ui->statusBar->showMessage("Start App.");
}

MainWindow::~MainWindow()
{
    m_console->deleteLater();
    delete ui;
}

void MainWindow::changePage(EPageIndexes pageIdx)
{
    bool pageAdded = m_pageIdx2StackWidgetIdx.contains(pageIdx);
    int stackWidgetIdx = 0;

    //updateLeftMenuIconsState();

    switch(pageIdx){
        case PAGE_IDX_WELCOME:
            if(!pageAdded){
                g_framework->g_pStartPage = new class StartPage(this);
                stackWidgetIdx = ui->stackedWidget->addWidget(g_framework->g_pStartPage);
                m_pageIdx2StackWidgetIdx.insert(pageIdx, stackWidgetIdx);
            }
            else{
                stackWidgetIdx = m_pageIdx2StackWidgetIdx.value(pageIdx);
            }
            ui->stackedWidget->setCurrentIndex(stackWidgetIdx);

        break;
        case PAGE_IDX_CONFIGURATION_MD04:

            if(!pageAdded){
                g_framework->g_pConfigMD04Page = new ConfigMD04Page(this);
                stackWidgetIdx = ui->stackedWidget->addWidget( g_framework->g_pConfigMD04Page );
                m_pageIdx2StackWidgetIdx.insert(pageIdx, stackWidgetIdx);
            }
            else{
                stackWidgetIdx = m_pageIdx2StackWidgetIdx.value(pageIdx);
            }
            ui->stackedWidget->setCurrentIndex(stackWidgetIdx);

            if(g_framework->g_pConfigMD04Page != nullptr)
            {
//                g_framework->g_ConfigurationPage->m_ConfigObj->loadFromFile(m_filePath);
//                g_framework->g_ConfigurationPage->setConfigDataToWidget();
            }

        break;
         /* case  PAGE_IDX_CALIBRATION_AP01:
            {
                if(!pageAdded)
                {
                    //g_framework->g_pCalibrationPage = new CalibrationPage(this);
                    stackWidgetIdx = ui->stackedWidget->addWidget(g_framework->g_pCalibrationPage);
                    m_pageIdx2StackWidgetIdx.insert(pageIdx,stackWidgetIdx);

                }
                else{
                    stackWidgetIdx = m_pageIdx2StackWidgetIdx.value(pageIdx);
                }
                ui->stackedWidget->setCurrentIndex(stackWidgetIdx);

            }
        break;*/

    case PAGE_IDX_TEST_CM03:
        if(!pageAdded)
        {
            g_framework->g_spmPage = new SPMpage(this);     //tworzenie strony testującej SPM
            stackWidgetIdx = ui->stackedWidget->addWidget(g_framework->g_spmPage);  //dodaj stronę
            m_pageIdx2StackWidgetIdx.insert(pageIdx,stackWidgetIdx);
        }
        else
            stackWidgetIdx = m_pageIdx2StackWidgetIdx.value(pageIdx);

        ui->stackedWidget->setCurrentIndex(stackWidgetIdx);
        break;
    }
}

void MainWindow::enableCM03Page()
{

}

//wlaczenie i wylaczenie konsoli
void MainWindow::on_consoleButton_toggled(bool checked)
{
    if(checked)
    {
        ui->consoleFrame->show();
        ui->consoleButton->setText(tr("HIDE CONSOLE"));
    }
    else
    {
        ui->consoleFrame->hide();
        ui->consoleButton->setText(tr("SHOW CONSOLE"));
    }
}

/** Wyswietlenie komunikatu z bledem (w formie dialogu) */
void MainWindow::showMsgBox(QString msg, QString title, int icon)
{
    qDebug() << "onSessionOpenError: " << msg;
    QMessageBox msgBox;
    if(title != "")
        msgBox.setWindowTitle(title);
    msgBox.setText(msg);
    msgBox.setIcon((QMessageBox::Icon)icon);
    msgBox.exec();
}
/** Dodaje wiadomosc do konsoli */
void MainWindow::addConsoleMessage(QString msg, int icon)
{
    m_console->addMsg(msg, icon);
}

//zdarzenie dla menu - zamkniecie aplikacji
void MainWindow::on_actionExit_triggered()
{
    QMainWindow::close();
}

void MainWindow::on_deviceButton_clicked()
{

    int selectedSourceTypeFile;

    // wybor rodzaju urzadzenia
    DeviceSelectDialog* devSelectDlg = new DeviceSelectDialog(this);
    int selectedDevice = devSelectDlg->myExec();
    devSelectDlg->deleteLater();
    if(selectedDevice == QDialog::Rejected)
        return;

    if(selectedDevice == MD04)
    {
        ui->configurationButton->setEnabled(true);
        this->changePage(MainWindow::PAGE_IDX_CONFIGURATION_MD04);
    }
}

//zdarzenie otwarcia okna konfiguracji - przycisk boczny
void MainWindow::on_configurationButton_clicked()
{
    this->changePage(PAGE_IDX_CONFIGURATION_MD04);
    //wlaczamy okno kalibracji i testowania
   // ui->calibrationButton->setEnabled(true);
}
//zdarzenie otwarcia okna kalibracji - przycisk boczny
void MainWindow::on_calibrationButton_clicked()
{
       //this->changePage(PAGE_IDX_CALIBRATION_AP01);
}
//zdarzenie otwarcia okna funkcji manualnych - przycisk boczny
void MainWindow::on_ManualButton_clicked()
{
 //   this->changePage(PAGE_IDX_MANUALFUNCIONS_AP01);
}

void MainWindow::on_actionSetup_triggered()
{

    //konfiguracja programu
    SoftwareSetup* softwareSetup = new SoftwareSetup(this);
    softwareSetup->setConfig(g_framework->g_configSoftware);

    bool retDialog = softwareSetup->myExec();
    if(retDialog == true)
    {
        g_framework->g_configSoftware->setValue(softwareSetup->getConfig());

        g_framework->g_modbusClientDevice->setPortName(g_framework->g_configSoftware->generalSettings.namePortsCom);
        g_framework->g_modbusClientDevice->m_settingsDevice = g_framework->g_configSoftware->generalSettings.settingsPortCom;

        ui->actionConnect->setEnabled(true);
    }
    softwareSetup->deleteLater();
}

void MainWindow::on_actionConnect_triggered()
{

    if(g_framework->g_modbusClientDevice->connectMDB() == true)
    {
        g_framework->g_connectStatus = true;
        ui->actionConnect->setEnabled(false);
        ui->actionSetup->setEnabled(false);
        //ui->calibrationButton->setEnabled(true);
//            if(g_framework->g_projectSet == true)
//                g_framework->g_CalibrationPage->setActiveButtons(true);


        ui->actionDisconnect->setEnabled(true);

    }
    else
    {
        g_framework->g_connectStatus = false;
        showMsgBox("Connection with Conntroler port: FAIL", "", QMessageBox::Warning);

    }
}

void MainWindow::on_actionDisconnect_triggered()
{

    g_framework->g_modbusClientDevice->disconnectMDB();

    g_framework->g_connectStatus = false;
    ui->actionConnect->setEnabled(true);
    ui->actionSetup->setEnabled(true);
    ui->actionDisconnect->setEnabled(false);

 //   g_framework->g_CalibrationPage->setActiveButtons(false);
}



void MainWindow::on_testDeviceButton_clicked()
{
    this->changePage(PAGE_IDX_TEST_CM03);
}
