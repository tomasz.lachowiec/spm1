#ifndef FRAMEWORK_H
#define FRAMEWORK_H


#include "mainwindow.h"

#include <QObject>
#include <QThread>
#include <QMutex>
#include <QDesktopWidget>

#include "Modbus/ModbusClient.h"
#include "Config/ConfigSoftware.h"

#include "GUI/Pages/StartPage.h"
#include "GUI/Pages/Modules/ConfigMD04Page.h"
#include "GUI/Pages/Modules/spmpage.h"

#include "ThreadWorker.h"
#include <MachineState.h>

class Framework : public QObject
{
    Q_OBJECT
public:
    explicit Framework(QObject *parent = nullptr);

    void centerMainWindow(QDesktopWidget *desktop);
    void createMainWindow(QDesktopWidget *desktop);

    QList<ThreadWorker*> g_workersList;                 // lista watkow
    void stop(bool majorChangesInCfg);

    class StartPage           *g_pStartPage = nullptr;
    class ConfigMD04Page      *g_pConfigMD04Page = nullptr;
    class CalibrationPage     *g_pCalibrationPage = nullptr;
    class SPMpage             *g_spmPage;

    ConfigSoftware  *g_configSoftware = nullptr;        //konfiguracja programu
    ModbusClient *g_modbusClientDevice  = nullptr;      //obiekt modbus master

    int g_connectionStatus;
    bool g_connectStatus = false;
    bool g_projectSet = false;


signals:

    void dialogMessage(QString msg, QString title, int icon); // icon to: QMessageBox::Icon
    void consoleMessage(QString msg, int icon = QMessageBox::NoIcon); // icon to: QMessageBox::Icon

public slots:


private:

    void runThreads(void);
    void runThread(ThreadWorker* worker, bool append2List);


    void stopThreads(void);
    void stopThread(ThreadWorker *worker);

    MainWindow* m_mainWindow;
};

#endif // FRAMEWORK_H
