#ifndef DEF_H
#define DEF_H

#include <QSerialPort>

// Definicje

#define MAX_READ_RECORDS    50000           //maksymalna ilosć odczytanych rekordow
#define MAX_IMPORT_READ_RECORDS    50000    //maksymalna ilosć odczytanych rekordow z bazy przy funkcji import

#define CFG_FILE_NAME "cfg.ini" // plik QSettings

// nazwa polaczenia do wew. bazy danych
#define INTERNAL_DB_CON_NAME "data_con"

#define PROGRESS_TASKS_DIVIDER 10  // dzielinik faktycznej liczby krokow postepu


//----------------------------------------
// FLAGI
//----------------------------------------
// domyslne standardowe flagi - (w konkretnym urzadzeniu mozna uzywac innych)
enum EStdFlagValues{
    STD_FLAG_VAL_OK  =       0,
    STD_FLAG_VAL_NOT_READY = 1, // jeszcze nie gotowy - wartosc wyswietlana + klepsydra
    STD_FLAG_VAL_ERROR     = 2, // bladna, wartosc zamieniana na "---", + ikona bledu
};

#define UNICODE_CHECK_MARK  "\u2714"    // ok: v
#define UNICODE_HOURGLASS   "\u231B"    // klepsyndra
#define UNICODE_CROSS_MARK  "\u2716"    // blad: X
//----------------------------------------

/** Jakie widoki sa dostepna dla sesji - uzywane jako maska bitowa */
enum avaibleViews_t{
    VIEW_CONFIGURATION_AP01 =  0x01,
    VIEW_CALIBRATION_AP01 =    0x02,
    VIEW_CHART =    0x04,
    VIEW_RAPORTS =  0x08,
    VIEW_LOGS =     0x10,
    VIEW_CSV_EXPORT = 0x20
    // 0x40
    // 0x80     short
    // 0x100    int
};

/*!
 * \brief The efuncReadRegister enum
 */
enum efuncReadRegister
{
    IDLE_FUNC = 0,
    INPUT_CONTROLLER_READ_FUNC = 1
};

enum avaibleDevices_t{
    DEVICE_UNDEFINED = 0,
    INTERNAL_DB = 1,
    FIRST_DEV_TYPE,
    MD04 = FIRST_DEV_TYPE, // !

    LAST_DEV_TYPE = MD04 // !
};


/*!
 * \brief The eReg4000 enum - Rejestry 4000
 */
enum eReg4000
{
    // Ustawienia interfejsu szeregowego USART1
    PORT_ADDR = 4000,				// adres w sieci Modbus
    PORT_BAUD,				// prędkosc transmisji
    PORT_FRAME,				// typ ramki
    PORT_TIMEOUT,			// okresla maksymalny czas w sekundach, przez który brak transmisji spowoduje przejscie wszystkich wyjsc i przełaczników do stanu bezpiecznego. Dla wartości zero funkcja wylaczona.
    PORT_APPLY,
    T_DS18B20_AVG_TIME,		// czas usredniania pomiaru w sekundach - aktualnie nie wykorzystywany!!!

    // Konfiguracja wejsc binarnych
    BIN_IN_TL1,		// minimalny czas trwania poziomu niskiego na wejsciu przed uznaniem go za poziom niski ( w milisekundach )
    BIN_IN_TH1,		// minimalny czas trwania poziomu wysokiego na wejsciu przed uznaniem go za poziom wysoki ( w milisekundach )
    BIN_IN_TL2,
    BIN_IN_TH2,
    BIN_IN_TL3,
    BIN_IN_TH3,
    BIN_IN_APPLY,	// powoduje pobranie konfiguracji czasów filtrowania
    // Konfiguracja wyjsc
    BIN_OUT_SAFE_STATE_O1,		// stan bezpieczny przyjmowany po uruchomieniu
    BIN_OUT_SAFE_STATE_O2,
    BIN_OUT_SAFE_STATE_O3,
    BIN_OUT_SAFE_STATE_O4,
    BIN_OUT_SAFE_STATE_O5,
    BIN_OUT_SAFE_STATE_O6,
    BIN_OUT_SAFE_STATE_O7,
    BIN_OUT_SAFE_STATE_O8,
    // Stany domyslne przełącznika JTAG i USART
    UART_SW_DEF_STATE,
    JTAG_SW_DEF_STATE,

    // rejestry zapisywane przez aplkację, które nie są pamiętane
    UART_SW_STATE,		// numer wybranego urządzenia dla UART - dla wartosci 0 wybrany jest kanał, który jest nigdzie nie podłączony
    JTAG_SW_STATE,		// numer wybranego urządzenia dla JTAG - dla wartosci 0 wybrany jest kanał, który jest nigdzie nie podłączony
    PK_CALIB1,			// stan przekaźników kalibratora rezystancji
    PK_CALIB2,
    PK_CALIB3,
    PK_CALIB4,
    PK_DEV,				// wybór urządzenia dla którego ma zostac dołączono rezystancja wzorca
    PK_CHANNEL,			// numer kalibrowanego kanału
    PK_MEAS,			// wymuszenie przełączenia kalibratora na zacisku pomiaru rezystancji przez multimetr wzorcowy. Ustawienie wartosci 1 powoduje odłączenie urządzenia i dołaczenie rwskazanego rezytora do zacisków pomiarowych oraz wyzerowanie przekaźnikow urzadzenia i kanalu.
                        // Po Wylaczeniu wymuszenia wyjscia wracaja do poprzedniego stanu
    BIN_OUT_1,			// stan wyjsc binarnych
    BIN_OUT_2,
    BIN_OUT_3,
    BIN_OUT_4,
    BIN_OUT_5,
    BIN_OUT_6,
    BIN_OUT_7,
    BIN_OUT_8,

    noReg4000,
};


/*!
 * \brief The eReg4100 enum - Rejestry 4100
 */
enum eRegRW
{
    REG_DEVICE_ID = 0,			// Identyfikator urządzenia
    REG_DEVICE_REV = 1,				// Wersja oprogramowania
    REG_STATUS_1 = 2,			// Status urządzenia
    REG_STATUS_2 = 3,
    // polecenia cmd
    REG_CMD_EN = 6,
    REG_CMD = 7,

    // ustawienia ethernet
    REG_IS_DHCP = 27,
    REG_IP_1,
    REG_IP_2,
    REG_MASK_1,
    REG_MASK_2,
    REG_GATEWAY_1,
    REG_GATEWAY_2,

    REG_MDB_ID,
    REG_MDB_PORT

};


enum eRegR
{

     ID_DEVICE = 0,			//identyfikator urzadzenia
     FIRMWARE_VER,	//wersja oprogramowania



};

enum eColorLed
{
    OFF_LED = 0,
    RED_LED,
    GREEN_LED,
    YELLOW_LED

};

enum eLed
{
    LED_1 = 0,
    LED_2,
    LED_3
};


typedef struct {
    int parity = QSerialPort::NoParity;
    int baud = QSerialPort::Baud115200;
    int dataBits = QSerialPort::Data8;
    int stopBits = QSerialPort::OneStop;
    int responseTime = 3000;
    int numberOfRetries = 2;
} SettingsDevice;


//-----------------------------------------------------
#endif // DEF_H
