#-------------------------------------------------
#
# Project created by QtCreator 2017-01-25T00:06:53
#
#-------------------------------------------------

QT          = core gui
QT          += serialbus serialport
QT          += network
QT          += charts
VERSION = 0.1.00
DEFINES += APP_VER=\\\"$$VERSION\\\"

CONFIG(debug, debug|release) {
  message("Debug")
}else{
  message("Release")
  DEFINES += QT_NO_DEBUG_OUTPUT
}


greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport




TARGET = configDevice
TEMPLATE = app
CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += main.cpp\
    Config/ConfigSoftware.cpp \
    GUI/Pages/Modules/candevice.cpp \
    GUI/Pages/Modules/infoform.cpp \
    GUI/Pages/Modules/serialportconnection.cpp \
    GUI/Pages/Modules/spmpage.cpp \
        mainwindow.cpp \
    Framework.cpp \
    ThreadWorker.cpp \
    Modbus/ModbusClient.cpp \
    Modbus/ModbusRegisters.cpp \
    MachineState.cpp \
    GUI/SoftwareSetup.cpp \
    GUI/Pages/Modules/ConfigMD04Page.cpp \
    GUI/Pages/StartPage.cpp \
    GUI/Views/DeviceSelectDialog.cpp \
    GUI/Widgets/ConsoleWidget.cpp \
    qcustomplot.cpp

HEADERS  += mainwindow.h \
    Config/ConfigSoftware.h \
    Framework.h \
    GUI/Pages/Modules/SerialPortConnection.h \
    GUI/Pages/Modules/candevice.h \
    GUI/Pages/Modules/infoform.h \
    GUI/Pages/Modules/spmpage.h \
    ThreadWorker.h \
    inc/def.h \
    inc/global.h \
    Modbus/ModbusClient.h \
    Modbus/ModbusRegisters.h \
    MachineState.h \
    GUI/SoftwareSetup.h \
    GUI/Pages/Modules/ConfigMD04Page.h \
    GUI/Pages/StartPage.h \
    GUI/Views/DeviceSelectDialog.h \
    GUI/Widgets/ConsoleWidget.h \
    qcustomplot.h

FORMS    += mainwindow.ui \
    GUI/Pages/Modules/candevice.ui \
    GUI/Pages/Modules/infoform.ui \
    GUI/Pages/Modules/serialportconnection.ui \
    GUI/Pages/Modules/spmpage.ui \
    GUI/Widgets/ConsoleWidget.ui \
    GUI/SoftwareSetup.ui \
    GUI/Pages/Modules/ConfigMD04Page.ui \
    GUI/Pages/StartPage.ui \
    GUI/Views/DeviceSelectDialog.ui

target.files = calibrateCup
target.path = /home/ubuntu/Qt_run
INSTALLS += target

RESOURCES += \
    resources.qrc

DISTFILES += \
    GUI/Pages/Modules/Plugins/plugin.json \
    todo.txt \
    rejestryController.txt \
    rejestryAP01.txt



win32:CONFIG(release, debug|release): LIBS += -L$$PWD/3rdparty/Libmodbus-build-release/ -lmodbus
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/3rdparty/Libmodbus-build-debug/ -lmodbus

INCLUDEPATH += $$PWD/3rdparty/libmodbus/src
DEPENDPATH += $$PWD/3rdparty/libmodbus/src
