#include "SoftwareSetup.h"
#include "ui_SoftwareSetup.h"

#include <QDir>
#include <QDebug>
#include <QSerialPortInfo>
#include <QSortFilterProxyModel>

#include "inc/global.h"

SoftwareSetup::SoftwareSetup(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SoftwareSetup)
{
    ui->setupUi(this);

    m_pathFile = QDir::currentPath()+ "/settings.ini";
    qDebug() << m_pathFile;
}

SoftwareSetup::~SoftwareSetup()
{
    delete ui;
}

bool SoftwareSetup::myExec()
{
    this->exec();
    return retConfigResult;
}

//pobrnie ustawionej konfiguracji
ConfigSoftware::GeneralSettings SoftwareSetup::getConfig(void)
{
    return m_configSoftware.generalSettings;
}

//ustawienie aktualnej konfiguracji
void SoftwareSetup::setConfig(ConfigSoftware *configSoftware)
{

     m_configSoftware.generalSettings.addresDevice = configSoftware->generalSettings.addresDevice;

     m_configSoftware.generalSettings.lang_Cfg = configSoftware->generalSettings.lang_Cfg;
     m_configSoftware.generalSettings.namePortsCom = configSoftware->generalSettings.namePortsCom;

     m_configSoftware.generalSettings.settingsPortCom = configSoftware->generalSettings.settingsPortCom;

     m_configSoftware.generalSettings.softwareDesc_Cfg = configSoftware->generalSettings.softwareDesc_Cfg;
     m_configSoftware.generalSettings.softwareName_Cfg = configSoftware->generalSettings.softwareName_Cfg;
     m_configSoftware.generalSettings.versionFirmware_Cfg = configSoftware->generalSettings.versionFirmware_Cfg;



//    m_configSoftware.generalSettings.addresDevice = configSoftware->generalSettings.addresDevice;
//    m_configSoftware.generalSettings.lang_Cfg = configSoftware->generalSettings.lang_Cfg;
//    m_configSoftware.generalSettings.namePortsCom = configSoftware->generalSettings.namePortsCom;
//    m_configSoftware.generalSettings.settingsPortCom = configSoftware->generalSettings.settingsPortCom;
//    m_configSoftware.generalSettings.softwareDesc_Cfg = configSoftware->generalSettings.softwareDesc_Cfg;
//    m_configSoftware.generalSettings.softwareName_Cfg = configSoftware->generalSettings.softwareName_Cfg;
//    m_configSoftware.generalSettings.versionFirmware_Cfg = configSoftware->generalSettings.versionFirmware_Cfg;

//    for(int i=0; i<16; i++)
//    {
//        m_configSoftware.generalSettings.channelsModulesRGB[i]->enable = configSoftware->generalSettings.channelsModulesRGB[i]->enable;
//        m_configSoftware.generalSettings.channelsModulesRGB[i]->modbusAddres = configSoftware->generalSettings.channelsModulesRGB[i]->modbusAddres;
//    }

     for (QSerialPortInfo port : QSerialPortInfo::availablePorts())
     {
         // print the port name
         qDebug() << port.portName();
         ui->portCOM->addItem(port.portName());
     }

//     QSortFilterProxyModel* proxy = new QSortFilterProxyModel(ui->portCOM_1);
//     proxy->setSourceModel( ui->portCOM_1->model());
//      ui->portCOM_1->model()->setParent(proxy);
//      ui->portCOM_1->setModel(proxy);
     ui->portCOM->model()->sort(0);
     ui->portCOM->setCurrentText(m_configSoftware.generalSettings.namePortsCom);

     ui->addresDevice->setValue(m_configSoftware.generalSettings.addresDevice);
     ui->baudCOM->setCurrentText(QString::number(m_configSoftware.generalSettings.settingsPortCom.baud));
     ui->retriesCOM->setValue(m_configSoftware.generalSettings.settingsPortCom.numberOfRetries);
     ui->responseDevice->setValue(m_configSoftware.generalSettings.settingsPortCom.responseTime);
    switch(m_configSoftware.generalSettings.settingsPortCom.dataBits)
    {
        case QSerialPort::Data7 :
            ui->wordCOM->setCurrentIndex(0);
            break;

        case QSerialPort::Data8 :
            ui->wordCOM->setCurrentIndex(1);
            break;
    }

    switch(m_configSoftware.generalSettings.settingsPortCom.parity)
    {
        case QSerialPort::NoParity :
            ui->parityCOM->setCurrentIndex(0);
            break;

        case QSerialPort::EvenParity :
            ui->parityCOM->setCurrentIndex(1);
            break;

        case QSerialPort::OddParity :
            ui->parityCOM->setCurrentIndex(2);
            break;
    }

    switch(m_configSoftware.generalSettings.settingsPortCom.stopBits)
    {
        case QSerialPort::OneStop :
            ui->stopCOM->setCurrentIndex(0);
            break;

        case QSerialPort::TwoStop :
            ui->stopCOM->setCurrentIndex(1);
            break;
    }
}


void SoftwareSetup::on_buttonBox_accepted()
{
    //pobieramy konfiguracje z kontrolek

//    ui->portCOM->setCurrentIndex(0);
//    ui->baudCOM->setCurrentIndex(7);
//    ui->wordCOM->setCurrentIndex(1);
//    ui->parityCOM->setCurrentIndex(2);
//    ui->stopCOM->setCurrentIndex(0);
//    ui->retriesCOM->setValue(2);
//    ui->responseDevice->setValue(1000);

//    m_configSoftware.generalSettings.namePortsCom = ui->portCOM->currentText();
//    m_configSoftware.generalSettings.addresDevice = ui->addresDevice->text().toInt();
//    m_configSoftware.generalSettings.settingsPortCom.baud = (int)ui->baudCOM->currentText().toInt();
//    m_configSoftware.generalSettings.settingsPortCom.responseTime = (int)ui->responseDevice->text().toInt();
//    m_configSoftware.generalSettings.settingsPortCom.numberOfRetries = (int)ui->retriesCOM->text().toInt();

//    switch(ui->wordCOM->currentIndex())
//    {
//        case 0 :
//            m_configSoftware.generalSettings.settingsPortCom.dataBits = QSerialPort::Data7;
//            break;

//        case 1 :
//            m_configSoftware.generalSettings.settingsPortCom.dataBits = QSerialPort::Data8;
//            break;
//    }

//    switch(ui->parityCOM->currentIndex())
//    {
//        case 0 :
//            m_configSoftware.generalSettings.settingsPortCom.parity = QSerialPort::NoParity;
//            break;

//        case 1 :
//            m_configSoftware.generalSettings.settingsPortCom.parity = QSerialPort::EvenParity;
//            break;

//        case 2 :
//            m_configSoftware.generalSettings.settingsPortCom.parity = QSerialPort::OddParity;
//            break;
//    }

//    switch(ui->stopCOM->currentIndex())
//    {
//        case  0 :
//            m_configSoftware.generalSettings.settingsPortCom.stopBits = QSerialPort::OneStop;
//            break;

//        case  1 :
//            m_configSoftware.generalSettings.settingsPortCom.stopBits = QSerialPort::TwoStop;
//            break;
//    }


//    if(m_configSoftware.save2File(m_pathFile) == ConfigSoftware::OK)
//    {
//        retConfigResult = true;
//    }
//    else
//    {
//        retConfigResult = false;
//    }

}

void SoftwareSetup::on_buttonBox_rejected()
{
    retConfigResult = false;
}
