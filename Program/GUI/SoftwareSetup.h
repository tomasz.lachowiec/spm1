#ifndef SOFTWARESETUP_H
#define SOFTWARESETUP_H

#include <QDialog>

#include "Config/ConfigSoftware.h"

namespace Ui {
class SoftwareSetup;
}

class SoftwareSetup : public QDialog
{
    Q_OBJECT

public:
    explicit SoftwareSetup(QWidget *parent = 0);
    ~SoftwareSetup();

    bool myExec();
    ConfigSoftware::GeneralSettings getConfig(void);
    void setConfig(ConfigSoftware *configSoftware);

private slots:

    void on_buttonBox_accepted();
    void on_buttonBox_rejected();

private:
    Ui::SoftwareSetup *ui;

    QString m_pathFile;

    ConfigSoftware  m_configSoftware;

    bool retConfigResult;
};

#endif // SOFTWARESETUP_H
