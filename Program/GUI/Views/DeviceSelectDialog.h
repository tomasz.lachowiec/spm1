#ifndef DEVICESELECTDIALOG_H
#define DEVICESELECTDIALOG_H

#include <QDialog>

namespace Ui {
class DeviceSelectDialog;
}

class DeviceSelectDialog : public QDialog
{
    Q_OBJECT

public:
    explicit DeviceSelectDialog(QWidget *parent = 0);
    ~DeviceSelectDialog();

    int myExec();

private slots:

    void on_MD04_button_clicked();

private:
    Ui::DeviceSelectDialog *ui;

    int selectedDevice;
};

#endif // DEVICESELECTDIALOG_H
