#include "StartPage.h"
#include "ui_StartPage.h"

StartPage::StartPage(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::StartPage)
{
    ui->setupUi(this);

    ui->versionLabel->setText(tr("Version ") + APP_VER);

}

StartPage::~StartPage()
{
    delete ui;
}
