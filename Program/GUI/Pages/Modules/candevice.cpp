#include "candevice.h"
#include "ui_candevice.h"

CanDevice::CanDevice(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::CanDevice)
{
    ui->setupUi(this);
   pConn = new SerialPortConnection;
    //canDeviceName = pConn->g_serial->portName(); //nazwa portu
    //rates = {10000, 20000, 50000, 100000, 125000, 500000, 1000000};
   // serialUi = new Ui::SerialPortConnection;

}

CanDevice::~CanDevice()
{
    delete ui;
}

void CanDevice::connectCAN(QVariant bitRate, QSerialPort *port)
{
    QString err;
    QString bitRateSend;

    if(bitRate==10000)
        bitRateSend = "S0\015";
    if(bitRate==20000)
        bitRateSend = "S1\015";
    if(bitRate==50000)
        bitRateSend = "S2\015";
    if(bitRate==100000)
        bitRateSend = "S3\015";
    if(bitRate==125000)
        bitRateSend = "S4\015";
    if(bitRate==200000)
        bitRateSend = "S5\015";
    if(bitRate==500000)
        bitRateSend = "S6\015";
    if(bitRate==800000)
        bitRateSend = "S7\015";
    if(bitRate==1000000)
        bitRateSend = "S8\015";

    QByteArray open_command[2] ;
    open_command->insert(0,'O');
    open_command->insert(1, "\n");
    char msg[1];
    sprintf(msg, "O\n");

    qDebug() << msg<< "\n";
    if(!port->isOpen())
        qDebug() << "Not open!";
    if(port->isOpen())
    {
        port->flush();      //wyslanie polecenia otwarcia połaczenia
        port->write(msg);
        port->waitForBytesWritten(50);

        if(port->isWritable())
            port->write(bitRateSend.toStdString().c_str());      //wyslij predkosc transmisji
    }
}

QString CanDevice::writeCanFrame(DataBuff dbuff, QSerialPort *port)
{
    //DataBuff dbuff;
    char dataToSend[7] = {'t'};
    //dataToSend[0] = 't';
    QByteArray frameID;
    QByteArray frameLen;
    int iterator=0;
    int id = dbuff.deviceID;
    int len = dbuff.frameLength;
    frameID.setNum(id,16);  //zamien id na hex

    if(dbuff.frameLength == len)
        dataToSend[0] = 't';        //ramka o standardowej dlugosci
    else
        dataToSend[0] = 'T';            //ramka rozszerzona

    //-------------tworzenie ramki o wyslania---------------

        for(iterator=0; iterator<frameID.length(); ++iterator)
                dataToSend[iterator+1] = (char) frameID[iterator];   //wstaw dane do bufora - dlugosc ramki

        ++iterator;

        dataToSend[iterator++] = (char)(len) + 0x30;

        for(int i = 0; i<dbuff.frameLength; ++i)
        {
            if(dbuff.data[i].length() > 1)
             {
                 dataToSend[iterator++] = dbuff.data[i].at(0);      //wyslij dane do bufora - wartosc funkcji
                 dataToSend[iterator++] = dbuff.data[i].at(1);
             }
             else {
                 dataToSend[iterator++] = 0x30;
                 dataToSend[iterator++] = dbuff.data[i].at(0);
              }
          }

        dataToSend[iterator++] = '\n';
        dataToSend[iterator] = '\0';
        qDebug() << dataToSend[0] << "\n";
        port->write(dataToSend);    //wyslij ramke
        port->waitForBytesWritten(50);
        port->flush();

}

QCanBusFrame::FrameType CanDevice::getErrorFrame(QCanBusFrame *frame)
{
    return frame->ErrorFrame;
}

QCanBusFrame CanDevice::readCanFrame()
{
   frame = canBusDevice->readFrame();
   if(canBusDevice->framesAvailable()){
        if(frame.error()){
            getErrorFrame(&frame);

         }
        else {
            return frame;
        }
   }
}

bool CanDevice::canState()
{
    if(canBusDevice->ConnectedState)
        return true;
    else if(canBusDevice->UnconnectedState)
        return false;
}

bool CanDevice::searchPlugin(QString name)
{
    QString error;
    QList<QString> plugins = QCanBus::instance()->plugins();


   // return true;
}

