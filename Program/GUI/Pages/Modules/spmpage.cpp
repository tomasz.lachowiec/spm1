#include "spmpage.h"
#include "ui_spmpage.h"
#include "ui_SoftwareSetup.h"
#include <QImage>
#include <QDebug>

SPMpage::SPMpage(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SPMpage)
{
    ui->setupUi(this);
    QImage image("C:/programyqt/OPCEVCCU/Produkcja/Program/CM03.bmp");
    ui->txtLogs->append(QDateTime::currentDateTime().toString("yyyy-MM-dd hh-mm") + " Aplication started. \n");
//    timer = new QTimer(this);
//    elapsedTimer = new QElapsedTimer;
//    timer->setInterval(1000);
//    connect(timer, SIGNAL(timeout()), this, SLOT(sendFrame("t126105")));
//    timer->start();
//    elapsedTimer->start();

//    timer = new QTimer(this);
//    qDebug() << p << "\n";
//  //  connect(ui->butConn, SIGNAL(clicked()), timer, SLOT(startTimer(1000)));

//    if(p==true){
//         connect(timer, SIGNAL(timeout()), this, SLOT(changeState()));
//         timer->start(1000);
//      }


    buttons.append(ui->relayButton);
    buttons.append(ui->relayButton2);
    buttons.append(ui->relayButton3);
    buttons.append(ui->relayButton4);
    buttons.append(ui->relayButton5);
    buttons.append(ui->relayButton6);

}

SPMpage::~SPMpage()
{
    delete ui;
}

void SPMpage::on_PP_Button_clicked()
{

}

void SPMpage::changeState()
{

    qDebug() << ile;
    qDebug() << buttons.at(1)->objectName() << "\n";
        buttons.at(ile)->setStyleSheet("QPushButton{background-color: green}");
        buttons.at(ile)->setStyleSheet("QPushButton{background-color: white}");
        sendFrame("1");
        ++ile;
        if(ile >= buttons.count())
            ile = 0;
//        timer->stop();
//        timer->start();
     qDebug() << ile;
}

void SPMpage::on_butConn_clicked()
{
    SerialPortConnection *serial = new SerialPortConnection;
    serial->setPortConfiguration();
    serial->show();
}

void SPMpage::sendFrame(QString value)
{
    if(portConn.g_serial->isOpen())
    {
        if(portConn.g_serial->isWritable())
        {
           portConn.g_serial->write(value.toStdString().c_str());
        }
    }
}

QString SPMpage::getFrame()
{
    if(portConn.g_serial->isOpen())
    {
        while(portConn.g_serial->canReadLine())
        {
            QString res = portConn.g_serial->readLine();
            QString terminate = "/r";
            int endOfLastElement = res.lastIndexOf(terminate);
            return res.left(endOfLastElement);
         }
    }
}

void SPMpage::connectCanDevice(QString portName, QSerialPort::BaudRate baud, QSerialPort::Parity parity, QSerialPort::DataBits databits, QSerialPort::StopBits stopbits, QVariant bitRate)
{

    qDebug() << portName << "\n";
    qDebug() << baud << "\n";
    qDebug() << parity  << "\n";
    qDebug() << databits << "\n";
    qDebug() << stopbits << "\n";

    portConn.g_serial = new QSerialPort(this);
    portConn.g_serial->setPortName(portName);
    portConn.g_serial->setBaudRate(baud);
    portConn.g_serial->setParity(parity);
    portConn.g_serial->setDataBits(databits);
    portConn.g_serial->setStopBits(stopbits);


    if(!portConn.g_serial->isOpen()){
        qDebug() << "ok \n";
        portConn.g_serial->open(QSerialPort::ReadWrite);
        canClient.connectCAN(bitRate, portConn.g_serial);
        connect(portConn.g_serial, SIGNAL(readyRead()), this, SLOT(getFrame()));
        ui->txtLogs->setStyleSheet("color: green");
        ui->txtLogs->append(QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm") + " Połączono z " + portConn.g_serial->portName() + "\n");
        timer = new QTimer(this);
        connect(timer, SIGNAL(timeout()), this, SLOT(timerTick()));
        timer->start(1000);
     }
    else
    {
        qDebug() << "OK";
        ui->txtLogs->setStyleSheet("color: red");
        ui->txtLogs->append(QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm") + " Nie można się połaczyć!" + "\n");
    }


    dBuff.deviceID = 126;
    dBuff.frameLength = 1;
    dBuff.data[0] = "05";

//    QByteArray p = "8";
    canClient.writeCanFrame(dBuff, portConn.g_serial);
    //qDebug() << dBuff->data[1] << "\n";
}

void SPMpage::disconnectDevice(QSerialPort *port)
{
    if(port->isOpen())
    {
        port->close();
        ui->txtLogs->setStyleSheet("color: green");
        ui->txtLogs->append(QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm") + " Port " + portConn.g_serial->portName() + " rozłączony! \n");
    }
    else
    {
       ui->txtLogs->setStyleSheet("color: red");
       ui->txtLogs->append(QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm") + " Nie można rozłączyć portu! \n");
    }
}

void SPMpage::timerTick()
{

//    QString data = canClient.writeCanFrame(126,"1",0x05);
//    connect(portConn.g_serial, SIGNAL(readyWrite()), this, SLOT(sendFrame(data)));
//    qDebug() << data << "\n";
//    sendFrame(data);
}

void SPMpage::on_butDisc_clicked()
{
    disconnectDevice(portConn.g_serial);
}

void SPMpage::on_txtLogs_textChanged()
{
//    QList<QString> logText;

//    QString code;
//    code = ui->txtLogs->toPlainText();

//    QStringList list = code.split(QRegExp("[\n]"),QString::KeepEmptyParts);
//    qDebug() << list << "\n";
//    qDebug() << list.length() << "\n";
//    for(int i=0; i<list.length();++i)
//    {
//         code = list[list.length()-1]+"\n";
//    }

//    if(code == "quit\n"){
//         this->close();
//     }
//    if(code == "connect\n"){
//        //connectCanDevice();
//     }
//     if(code == "disconnect\n"){
//         disconnectDevice();
//     }


}
