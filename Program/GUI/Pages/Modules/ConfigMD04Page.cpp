#include "ConfigMD04Page.h"
#include "ui_ConfigMD04Page.h"

#include "inc/global.h"
#include <QHostAddress>

#include <QDebug>

ConfigMD04Page::ConfigMD04Page(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ConfigMD04Page)
{
    ui->setupUi(this);

    if(m_ConfigSoftware != nullptr)
   {
       delete m_ConfigSoftware;
   }

   ui->tab_configDevice->setTabEnabled(1,false);
   m_ConfigSoftware = new ConfigSoftware();  //utworzenie nowej konfiguracji
}

ConfigMD04Page::~ConfigMD04Page()
{
    delete ui;
}

void ConfigMD04Page::on_enableDHCP_clicked(bool checked)
{
    if(checked == true)
    {
        emit g_framework->consoleMessage( tr("DHCP enable") );
        ui->ipAddress->setEnabled(false);
        ui->netMask->setEnabled(false);
        ui->ipGateway->setEnabled(false);

        m_configEthernetPrm.isDHCP = true;
    }
    else {
        emit g_framework->consoleMessage( tr("DHCP disable") );
        ui->ipAddress->setEnabled(true);
        ui->netMask->setEnabled(true);
        ui->ipGateway->setEnabled(true);

        m_configEthernetPrm.isDHCP = false;
    }
}


void ConfigMD04Page::setConfigDataToWidget(void)
{



}

void ConfigMD04Page::setWidgetDataToConfig(void)
{



}



void ConfigMD04Page::on_readCommunicationConfigButton_clicked()
{

    if(g_framework->g_connectStatus == true)
    {
        emit g_framework->consoleMessage( tr("Read parameters from device") );

        QList<uint16_t> registersList = g_framework->g_modbusClientDevice->readRegisters(1, (int)REG_IS_DHCP, 9, 1);

        qDebug() << registersList.length();

        if(registersList.length() == 9)
        {
            if(registersList.at(REG_IS_DHCP-REG_IS_DHCP) == 1)
            {
              ui->ipAddress->setEnabled(false);
              ui->netMask->setEnabled(false);
              ui->ipGateway->setEnabled(false);
              ui->enableDHCP->setChecked(true);
            }
            else
            {
                ui->ipAddress->setEnabled(true);
                ui->netMask->setEnabled(true);
                ui->ipGateway->setEnabled(true);
                ui->enableDHCP->setChecked(false);
            }

            uint8_t ip[4];
            ip[0] = (uint8_t)(registersList.at(REG_IP_1-REG_IS_DHCP) & 0xFF);
            ip[1] = (uint8_t)((registersList.at(REG_IP_1-REG_IS_DHCP) >> 8) & 0xFF);

            ip[2] = (uint8_t)(registersList.at(REG_IP_2-REG_IS_DHCP) & 0xFF);
            ip[3] = (uint8_t)((registersList.at(REG_IP_2-REG_IS_DHCP) >> 8) & 0xFF);

            QString ipAddress  =  QString::number(ip[0]) + '.' + QString::number(ip[1]) + '.' + QString::number(ip[2]) + '.' + QString::number(ip[3]);

            ui->ipAddress->setText(ipAddress);

            uint8_t mask[4];
            mask[0] = (uint8_t)(registersList.at(REG_MASK_1-REG_IS_DHCP) & 0xFF);
            mask[1] = (uint8_t)((registersList.at(REG_MASK_1-REG_IS_DHCP) >> 8) & 0xFF);

            mask[2] = (uint8_t)(registersList.at(REG_MASK_2-REG_IS_DHCP) & 0xFF);
            mask[3] = (uint8_t)((registersList.at(REG_MASK_2-REG_IS_DHCP) >> 8) & 0xFF);

            QString ipMask  =  QString::number(mask[0]) + '.' + QString::number(mask[1]) + '.' + QString::number(mask[2]) + '.' + QString::number(mask[3]);

            ui->netMask->setText(ipMask);

            uint8_t gateway[4];
            gateway[0] = (uint8_t)(registersList.at(REG_GATEWAY_1-REG_IS_DHCP) & 0xFF);
            gateway[1] = (uint8_t)((registersList.at(REG_GATEWAY_1-REG_IS_DHCP) >> 8) & 0xFF);

            gateway[2] = (uint8_t)(registersList.at(REG_GATEWAY_2-REG_IS_DHCP) & 0xFF);
            gateway[3] = (uint8_t)((registersList.at(REG_GATEWAY_2-REG_IS_DHCP) >> 8) & 0xFF);

            QString ipGateway  =  QString::number(gateway[0]) + '.' + QString::number(gateway[1]) + '.' + QString::number(gateway[2]) + '.' + QString::number(gateway[3]);

            ui->ipGateway->setText(ipGateway);

            ui->modbusID->setText(QString::number(registersList.at(REG_MDB_ID-REG_IS_DHCP)));
            ui->modbusPort->setText(QString::number(registersList.at(REG_MDB_PORT-REG_IS_DHCP)));


        }
        else
        {
            emit g_framework->consoleMessage( tr("Read parameters fail"), QMessageBox::Critical );
        }
    }
    else
    {
         emit g_framework->consoleMessage( tr("No open serial Port"), QMessageBox::Critical);
    }
}

void ConfigMD04Page::on_writeCommunicationConfigButton_clicked()
{
    if(g_framework->g_connectStatus == true)
    {
        emit g_framework->consoleMessage( tr("Write parameters to device") );

        uint16_t regBuf[9];
        QStringList ipAddressList;
        QStringList netMaskList;
        QStringList ipGatewayList;

        //odczytujemy adres ip
        QHostAddress address(ui->ipAddress->text());
        if (QAbstractSocket::IPv4Protocol == address.protocol())
        {
            ipAddressList = ui->ipAddress->text().split('.');
            if(ipAddressList.length() != 4)
            {
                emit g_framework->consoleMessage( tr("Unknown or invalid address."), QMessageBox::Critical  );
                //mozna oznaczyc kontrolke na czerwono lub postawic wykrzyknik obok
                return;
            }

            emit g_framework->consoleMessage( tr("Valid IPv4 address.") );
        }
        else
        {
           emit g_framework->consoleMessage( tr("Unknown or invalid address."), QMessageBox::Critical  );
        }

        //odczytujemy maskę
        QHostAddress mask(ui->netMask->text());
        if (QAbstractSocket::IPv4Protocol == mask.protocol())
        {
            netMaskList = ui->netMask->text().split('.');
            if(netMaskList.length() != 4)
            {
                emit g_framework->consoleMessage( tr("Unknown or invalid address."), QMessageBox::Critical  );
                //mozna oznaczyc kontrolke na czerwono lub postawic wykrzyknik obok
                return;
            }

            emit g_framework->consoleMessage( tr("Valid mask address.") );
        }
        else
        {
           emit g_framework->consoleMessage( tr("Unknown or invalid address."), QMessageBox::Critical  );
        }

        //odczytujemy gateway
        QHostAddress gateway(ui->netMask->text());
        if (QAbstractSocket::IPv4Protocol == gateway.protocol())
        {
            ipGatewayList = ui->ipGateway->text().split('.');
            if(ipGatewayList.length() != 4)
            {
                emit g_framework->consoleMessage( tr("Unknown or invalid address."), QMessageBox::Critical  );
                //mozna oznaczyc kontrolke na czerwono lub postawic wykrzyknik obok
                return;
            }

            emit g_framework->consoleMessage( tr("Valid gateway address.") );
        }
        else
        {
           emit g_framework->consoleMessage( tr("Unknown or invalid address."), QMessageBox::Critical  );
        }
        //wypenimy tablice aby potem zapisac wartosci do rejestrow

        if(ui->enableDHCP->isChecked() == true)
            regBuf[0] = 1;
        else
            regBuf[0] = 0;

        regBuf[1] = ipAddressList.at(1).toUShort() << 8 | ipAddressList.at(0).toUShort();
        regBuf[2] = ipAddressList.at(3).toUShort() << 8 | ipAddressList.at(2).toUShort();

        regBuf[3] = netMaskList.at(1).toUShort() << 8 | netMaskList.at(0).toUShort();
        regBuf[4] = netMaskList.at(3).toUShort() << 8 | netMaskList.at(2).toUShort();

        regBuf[5] = ipGatewayList.at(1).toUShort() << 8 | ipGatewayList.at(0).toUShort();
        regBuf[6] = ipGatewayList.at(3).toUShort() << 8 | ipGatewayList.at(2).toUShort();

        regBuf[7] = ui->modbusID->text().toUShort();
        regBuf[8] = ui->modbusPort->text().toUShort();

        //zapis parametrow do rejestrow
        if(g_framework->g_modbusClientDevice->writeRegisters(1, (int)REG_IS_DHCP, regBuf, 9, 1) == true)
        {
            regBuf[0] = 112;
            regBuf[1] = func_writeAllCfg;
            //zapis parametrow do eeperomu
            if(g_framework->g_modbusClientDevice->writeRegisters(1, (int)REG_CMD_EN, regBuf, 2, 1) == true)
            {
                emit g_framework->consoleMessage( tr("Configuration written") );

            }
            else
            {
                 emit g_framework->consoleMessage( tr("Configuration failed"), QMessageBox::Critical );
            }


        }
        else
        {
            emit g_framework->consoleMessage( tr("Configuration failed"), QMessageBox::Critical  );
        }
    }
    else
    {
         emit g_framework->consoleMessage( tr("No open serial Port"), QMessageBox::Critical);
    }

}

void ConfigMD04Page::on_resetDeviceButton_clicked()
{
    if(g_framework->g_connectStatus == true)
    {
        emit g_framework->consoleMessage( tr("Write parameters to device") );

        uint16_t regBuf[9];

        regBuf[0] = 112;
        regBuf[1] = func_resetDecvice;
        //reset urzadzenia
        if(g_framework->g_modbusClientDevice->writeRegisters(1, (int)REG_CMD_EN, regBuf, 2, 1) == true)
        {
            emit g_framework->consoleMessage( tr("Device resetting"));

        }
        else
        {
             emit g_framework->consoleMessage( tr("Reset device fail"), QMessageBox::Critical );
        }
    }
    else
    {
         emit g_framework->consoleMessage( tr("No open serial Port"), QMessageBox::Critical);
    }
}
