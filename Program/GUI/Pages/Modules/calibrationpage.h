#ifndef CALIBRATIONPAGE_H
#define CALIBRATIONPAGE_H

#include <QWidget>
#include <QtCharts>
#include <QtMath>
#include <QTimer>
#include "qcustomplot.h"
#include "Modbus/ModbusClient.h"
#include "infoform.h"
#include "../../Widgets/ConsoleWidget.h"
#include "Config/ConfigSoftware.h"
namespace Ui {
class CalibrationPage;
}

class CalibrationPage : public QWidget
{
    Q_OBJECT

public:
    explicit CalibrationPage(QWidget *parent = nullptr);
    ~CalibrationPage();

    int x = 0;
    int a = 0;


    enum {func_error = -1, func_idle = 0, func_readAllCfg = 1,
                                                func_writeAllCfg = 2,
                                                func_setDefaultCfg = 3,
                                                func_resetDecvice = 4,
                                                func_readCalPrmToReg = 101,
                                                func_calVoltage = 10,
                                                func_calVoltage_Point1 = 11,
                                                func_calVoltage_Point2 = 12,
                                                func_calCurrent = 20,
                                                func_calCurrent_Point1 = 21,
                                                func_calCurrent_Point2 = 22,
                                                func_writeCalPrm = 100
    };

    enum {code_configuration = 112, code_production = 36};


private slots:
    void on_bCalib_clicked();

    void on_btemp1_setP1_clicked();

    void on_btemp1_setP2_clicked();

    void on_btemp1_setP1_2_clicked();

    void on_btemp1_setP2_2_clicked();

    void on_btemp2_setP1_clicked();

    void on_btemp2_setP2_clicked();

    bool connectToDevice();
    void on_bDisc_clicked();

    void on_bCalibTemp1_clicked();

    void on_bCalibTemp2_clicked();

    void on_bCalibCP_clicked();

    void on_bCalibCP_Minus_clicked();

    void on_bTemp2_setP2_clicked();

    void on_btemp1_setPMin_clicked();

    void on_btemp1_setPMax_clicked();

    void on_btemp1_setPkt2_clicked();
    void timerTick();
    void createInfoForm();

    void measureValues();
    void on_bMeausre_clicked();

    void on_bMeausre_2_clicked();

    void on_bCreatePlot_clicked();

private:
    Ui::CalibrationPage *ui;
    QTimer *timer;
    QElapsedTimer  *eTimer;
    QTimer *measureTimer;

    InfoForm *infoForm;
    int t = 0;
    ConsoleWidget *console;
   // Framework *g_framework;

    uint16_t production_code = code_production;

    uint16_t ziarno = 0;
    void checkTextBox(QString pointX, QString pointY, QPushButton *calibButton, QPushButton *plotButton);
    void sendData(QString parametr);

    uint16_t getRegisters(int rej);
    void kalibracjaWartosci(int x, int y);
    bool ifDisconnected = false;

     //tablica przechowujaca wspolczynniki i punkty kalibracji
    QList<QPushButton*> buttonList;

    float wspolA, wspolB;
};




#endif // CALIBRATIONPAGE_H
