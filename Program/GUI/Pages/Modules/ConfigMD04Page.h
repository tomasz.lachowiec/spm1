#ifndef CONFIGMD04PAGE_H
#define CONFIGMD04PAGE_H

#include <QDialog>

#include "Config/ConfigSoftware.h"

namespace Ui {
class ConfigMD04Page;
}

class ConfigMD04Page : public QDialog
{
    Q_OBJECT

public:
    explicit ConfigMD04Page(QWidget *parent = nullptr);
    ~ConfigMD04Page();

    ConfigSoftware   *m_ConfigSoftware = nullptr; //konfiguracja programu

    void setConfigDataToWidget(void);
    void setWidgetDataToConfig(void);


private slots:
    void on_enableDHCP_clicked(bool checked);

    void on_readCommunicationConfigButton_clicked();

    void on_writeCommunicationConfigButton_clicked();

    void on_resetDeviceButton_clicked();

private:
    Ui::ConfigMD04Page *ui;

    //konfiguracja interfejsu ETH
    class ConfigEthernetPrm{
    public:
        ConfigEthernetPrm() : isDHCP(0), modbusID(1), modbusPort(502) {
            memset(ipAddr, 0, sizeof(ipAddr));
            memset(netMask, 0, sizeof(netMask));
            memset(ipGateway, 0, sizeof(ipGateway));
        }

        uint16_t isDHCP;
        uint16_t ipAddr[2];
        uint16_t netMask[2];
        uint16_t ipGateway[2];

        uint16_t modbusID;
        uint16_t modbusPort;
    };

    ConfigEthernetPrm   m_configEthernetPrm;

    enum {func_error = -1, func_idle = 0, func_readAllCfg = 1,
                                                func_writeAllCfg = 2,
                                                func_setDefaultCfg = 3,
                                                func_resetDecvice = 4,
                                                func_writeSerialNo = 100
    };


};

#endif // CONFIGMD04PAGE_H
