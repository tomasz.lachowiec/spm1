#ifndef CANDEVICE_H
#define CANDEVICE_H

#include <QWidget>
#include <QDebug>
#include <QCanBus>
#include <QCanBusDevice>
#include <QCanBusDeviceInfo>
#include <QCanBusFrame>
#include <QCanBusFactoryV2>
#include <QtEndian>
#include <memory>
#include "SerialPortConnection.h"
#define STANDARDLEN 8

namespace Ui {
class CanDevice;
}

class CanDevice : public QWidget
{
public:
    explicit CanDevice(QWidget *parent = nullptr);
    ~CanDevice();
    SerialPortConnection *pConn;

     void connectCAN(QVariant bitRate, QSerialPort *port);
    enum{
         BasicStandard,
         ExtendedStandard
     }frameType;

    enum{
        frame_id,
        payload
    };

   typedef struct DataBuff{
       int deviceID;
       int frameLength;
       QByteArray data[8];
    }dBuff;



      QString writeCanFrame(DataBuff dbuff, QSerialPort *port);
private:
    Ui::CanDevice *ui;

    void disconnectCAN();
    QCanBusFrame::FrameType getErrorFrame(QCanBusFrame *frame);
    QCanBusFrame readCanFrame();
    bool canState();
    QString convertToASCII();
    bool searchPlugin(QString name);
    QList<int>rates;
    QCanBusDevice *canBusDevice;
    QString canDeviceName;
    std::unique_ptr<QCanBusDevice> devices;
    QCanBusFrame frame;
};

#endif // CANDEVICE_H
