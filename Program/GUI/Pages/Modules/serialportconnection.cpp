#include "SerialPortConnection.h"
#include "ui_serialportconnection.h"
#include "spmpage.h"
SerialPortConnection::SerialPortConnection(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SerialPortConnection)
{
    ui->setupUi(this);
   // setPortConfiguration();
}

SerialPortConnection::~SerialPortConnection()
{
    delete ui;
}

void SerialPortConnection::setPortConfiguration()
{
    serialports = QSerialPortInfo::availablePorts();

    for(int i = 0; i<serialports.length(); ++i)
        ui->portCOM->addItem(serialports.at(i).portName());

    ui->cbBaud->addItem("1200");
    ui->cbBaud->addItem("2400");
    ui->cbBaud->addItem("4800");
    ui->cbBaud->addItem("9600");

    ui->cbBaud->addItem("19200");
    ui->cbBaud->addItem("38400");
    ui->cbBaud->addItem("115200");
    ui->cbBaud->setCurrentIndex(6);

    ui->cbStop->addItem("1");
    ui->cbStop->addItem("2");

    ui->cbWord->addItem("7");
    ui->cbWord->addItem("8");
    ui->cbWord->setCurrentIndex(0);

    ui->cbParity->addItem("NONE");
    ui->cbParity->addItem("ODD");
    ui->cbParity->addItem("EVEN");
    ui->cbParity->setCurrentIndex(0);

    ui->cbBitRate->addItem("10000");
    ui->cbBitRate->addItem("20000");
    ui->cbBitRate->addItem("50000");
    ui->cbBitRate->addItem("100000");
    ui->cbBitRate->addItem("125000");
    ui->cbBitRate->addItem("500000");
    ui->cbBitRate->addItem("1000000");
    ui->cbBitRate->setCurrentIndex(4);
}

void SerialPortConnection::configureDevice()
{
    QString portName;
    SPMpage spmpage;

    portName = ui->portCOM->currentText(); //port name
    QSerialPort::BaudRate baud;
    QSerialPort::Parity parity;
    QSerialPort::StopBits stopbits;
    QSerialPort::DataBits databits;

    switch(ui->cbParity->currentIndex())
    {
        case 0:
            parity = QSerialPort::NoParity;
        break;

        case 1:
            parity = QSerialPort::EvenParity;
        break;
        case 2:
            parity = QSerialPort::OddParity;
          break;
    }

    switch(ui->cbStop->currentIndex())
    {
        case 0:
            stopbits = QSerialPort::OneStop;
         break;

        case 1:
            stopbits = QSerialPort::TwoStop;
         break;
    }

    switch(ui->cbWord->currentIndex())
    {
        case 0:
            databits = QSerialPort::Data7;
           break;
        case 1:
            databits = QSerialPort::Data8;
        break;
    }

    switch(ui->cbBaud->currentIndex())
    {
        case 0:
            baud = QSerialPort::Baud1200;
        break;
        case 1:
            baud = QSerialPort::Baud2400;
        break;
        case 2:
           baud = QSerialPort::Baud4800;
        break;
        case 3:
            baud = QSerialPort::Baud9600;
        break;
        case 4:
            baud = QSerialPort::Baud19200;
        break;
        case 5:
            baud = QSerialPort::Baud38400;
        break;
        case 6:
            baud = QSerialPort::Baud115200;
        break;

    }
   // ui->cbBitRate->setCurrentIndex()
    for(int i=0; i<ui->cbBitRate->count(); ++i)
        canBitRate.append(ui->cbBitRate->currentText().toInt());

    QVariant selectedRate = QVariant(ui->cbBitRate->currentText().toInt());
    spmpage.connectCanDevice(portName,baud,parity,databits,stopbits,selectedRate);
}

void SerialPortConnection::on_bSerialSet_accepted()
{

    setPortConfiguration();
    configureDevice();
    this->close();
}

void SerialPortConnection::on_bSerialSet_rejected()
{
    this->close();
}
