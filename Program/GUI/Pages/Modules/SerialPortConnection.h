#ifndef SERIALPORTCONNECTION_H
#define SERIALPORTCONNECTION_H

#include <QWidget>
#include <QSerialPort>
#include <QSerialPortInfo>
#include <QCanBus>
#include <QCanBusDeviceInfo>
#include <QDebug>

namespace Ui {
class SerialPortConnection;
}

class SerialPortConnection : public QWidget
{
    Q_OBJECT

public:
    explicit SerialPortConnection(QWidget *parent = nullptr);
    ~SerialPortConnection();
    void setPortConfiguration();
    void configureDevice();
    QSerialPort *g_serial;
    QList<int> canBitRate;

private slots:
    void on_bSerialSet_accepted();
    void on_bSerialSet_rejected();

private:
    Ui::SerialPortConnection *ui;
    QList<QSerialPort::BaudRate> baudRates;
    QList<QSerialPortInfo> serialports;

};

#endif // SERIALPORTCONNECTION_H
