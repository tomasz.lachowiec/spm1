#ifndef SPMPAGE_H
#define SPMPAGE_H

#include <QWidget>
#include <QPushButton>
#include <QTimer>
#include <QSerialPort>
#include <QSerialPortInfo>
#include <QDateTime>
#include "GUI/SoftwareSetup.h"
#include "GUI/Pages/Modules/SerialPortConnection.h"
#include "GUI/Pages/Modules/candevice.h"

namespace Ui {
class SPMpage;
}

class SPMpage : public QWidget
{
    Q_OBJECT

public:
    int ile = 0;
    explicit SPMpage(QWidget *parent = nullptr);
    ~SPMpage();
    SerialPortConnection portConn;
    CanDevice canClient;
    CanDevice::DataBuff dBuff;

private slots:
    void on_PP_Button_clicked();
    void changeState();

    void on_butDisc_clicked();

    void on_txtLogs_textChanged();
    void on_butConn_clicked();
    void timerTick();
     QString getFrame();


private:
    Ui::SPMpage *ui;
    QList<QPushButton *> buttons;
    QTimer *timer;
    void sendFrame(QString value);
    SoftwareSetup *softwareSetup;
    QList<QSerialPortInfo> availableDevices;
    QElapsedTimer *elapsedTimer;
    void disconnectDevice(QSerialPort *port);
    bool p=false ;



 public:  void connectCanDevice(QString portName, QSerialPort::BaudRate baud, QSerialPort::Parity parity, QSerialPort::DataBits databits, QSerialPort::StopBits stopbits, QVariant bitRate);
};

#endif // SPMPAGE_H
