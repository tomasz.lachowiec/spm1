#include "calibrationpage.h"
#include "ui_calibrationpage.h"

ModbusClient *client;
CalibrationPage::CalibrationPage(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::CalibrationPage)
{
    ui->setupUi(this);
    ui->bDisc->setEnabled(0);
    QList< QSerialPortInfo> porty;
    porty = QSerialPortInfo::availablePorts();

    for(int i=0; i<porty.count(); ++i)
        ui->wyborPortu->addItem(porty.at(i).portName());

    ui->wyborWYkresu->addItem("TEMP1");
    ui->wyborWYkresu->addItem("TEMP2");
    ui->wyborWYkresu->addItem("CP");
}


CalibrationPage::~CalibrationPage()
{
    delete ui;
}

void CalibrationPage::on_bCalib_clicked()
{
    connectToDevice();
}

void CalibrationPage::on_btemp1_setP1_clicked()
{

    createInfoForm();
    ui->btemp1_setPkt2->setEnabled(1);
    ui->btemp1_setP1->setEnabled(0);
    ui->btemp2_setP1->setEnabled(0);
    ui->btemp1_setPMin->setEnabled(0);
    ui->btemp1_setPMax->setEnabled(0);
    sendData("41");
    sendData("100");

}
void CalibrationPage::sendData(QString parametr)
{

    uint16_t fcn;

    if(parametr == "10")
    {
        fcn = 10;
        client->writeRegisters(1,7,&fcn,1,0);

    }
    else if( parametr == "11")
    {
        fcn = 11;
        client->writeRegisters(1,7,&fcn,1,0);
    }
    else if(parametr == "12")
    {
        fcn = 12;
        client->writeRegisters(1,7,&fcn,1,0);
    }
    else if(parametr == "20")
    {
        fcn = 20;
        client->writeRegisters(1,7,&fcn,1,0);
    }
    else if(parametr == "21")
    {
        fcn = 21;
        client->writeRegisters(1,7,&fcn,1,0);
    }
    else if(parametr == "22")
    {
        fcn = 22;
        client->writeRegisters(1,7,&fcn,1,0);
    }
    else if(parametr == "40")
    {
        fcn = 40;
        client->writeRegisters(1,7,&fcn,1,0);
        uint16_t res = getRegisters(8);
        ui->lcdTemp1->display(res);
        qDebug() << res << "\n";
    }
    else if(parametr == "41")
    {
        fcn = 41;
        client->writeRegisters(1,7,&fcn,1,0);
    }
    else if(parametr == "42")
    {
        fcn = 42;
        client->writeRegisters(1,7,&fcn,1,0);
        fcn = ui->temp2_pkt1_max->text().toInt();
        client->writeRegisters(1,77,&fcn,1,0);
    }
    else if(parametr == "50")
    {
       fcn = 50;
       client->writeRegisters(1,7,&fcn,1,0);
       uint16_t res = getRegisters(10);
       ui->lcd_Temp2->display(res);
       qDebug() << res << "\n";
    }
    else if(parametr == "51")
    {
        fcn = 51;
        client->writeRegisters(1,7,&fcn,1,0);
    }
    else if(parametr == "52")
    {
        fcn = 52;
        client->writeRegisters(1,7,&fcn,1,0);
    }
    else if(parametr == "100")
    {
        fcn = 100;
        client->writeRegisters(1,7,&fcn,1,0);
    }
    else if(parametr == "101")
    {
        fcn = 101;
        client->writeRegisters(1,7,&fcn,1,0);
    }

}

uint16_t CalibrationPage::getRegisters(int rej)
{
     QList <uint16_t>odp;

        if(rej == 6)        //30007
            odp =  client->readRegisters(1,rej, 1, 1);
        else if(rej == 8)   //30009
            odp =  client->readRegisters(1,rej, 1, 1);
        else if(rej ==10)   //30011
            odp =  client->readRegisters(1,rej, 1, 1);
        else if(rej ==12)                       //300013
            odp =  client->readRegisters(1,rej, 1, 1);
        else if(rej ==13)           //300014
            odp =  client->readRegisters(1,rej, 1, 1);
        else if(rej ==14)       //300015
            odp =  client->readRegisters(1,rej, 1, 1);
        else if(rej ==76)
            odp =  client->readRegisters(1,rej, 1, 1);
        else if(rej ==77)
            odp =  client->readRegisters(1,rej, 1, 1);
        else if(rej ==78)
            odp =  client->readRegisters(1,rej, 1, 1);

    return odp.at(0);
}

void CalibrationPage::kalibracjaWartosci(int x, int y)
{
    ConsoleWidget *console = new ConsoleWidget;
    float wspolA = (4095)/(y-x);
    float wspolB = y-wspolA*x;
    console->addMsg("Wartosc wspol. a = " + QString::number(wspolA) + " wartosc wspol. b = " + QString::number(wspolB) + " \n ",QMessageBox::NoIcon);
    qDebug() << "Wartosc wspol. a = "  << QString::number(wspolA) <<  " wartosc wspol. b = " <<  QString::number(wspolB) << " \n ";
}

void CalibrationPage::on_btemp1_setP1_2_clicked()
{
    ui->bCalibCP_Minus->setEnabled(1);
    ui->btemp1_setP1_2->setEnabled(0);
    createInfoForm();
    sendData("12");
    sendData("100");
}

void CalibrationPage::on_btemp1_setP2_2_clicked()
{
    ui->bCalibCP->setEnabled(1);
    ui->btemp1_setP2_2->setEnabled(0);
    createInfoForm();
    sendData("22");
    sendData("100");
}

void CalibrationPage::on_btemp2_setP1_clicked()
{
    ui->btemp2_setP1->setEnabled(0);
    ui-> bTemp2_setP2->setEnabled(1);
    ui->btemp1_setPMin->setEnabled(0);
    ui->btemp1_setPMax->setEnabled(0);
    ui->btemp1_setP1->setEnabled(0);

    createInfoForm();
    sendData("51");
    sendData("100");
}

void CalibrationPage::on_btemp2_setP2_clicked()
{
    ui->bCalibTemp2->setEnabled(1);
    createInfoForm();
    sendData("52");
    sendData("100");
}

bool CalibrationPage::connectToDevice()
{

    QString portName = ui->wyborPortu->currentText();
    client = new ModbusClient(portName,ModbusClient::ModbusConnection::Serial,0);
    client->connectMDB();

    if(client->isConnected == true)
    {
        console = new ConsoleWidget(this);
        client->writeRegisters(1,6, &production_code,1,0);
        ui->bCalib->setEnabled(0);
        ui->bDisc->setEnabled(1);
        ui->btemp2_setP1->setEnabled(1);
        ui->btemp1_setP1->setEnabled(1);

        ui->bMeausre->setEnabled(1);
        ui->btemp1_setPMin->setEnabled(1);
        ui->labInfo->setText("Połączono z: " + ui->wyborPortu->currentText());
        console->addMsg("Polaczono z: " + ui->wyborPortu->currentText(), QMessageBox::NoIcon);
        console->update();
        return true;
     }

    else
    {
        ui->labInfo->setText("Błąd połączenia!");
        ui->bCalib->setEnabled(1);
        ui->bMeausre->setEnabled(0);
        ui->btemp2_setP1->setEnabled(0);
        ui->btemp1_setP1->setEnabled(0);
        ui->btemp1_setPMin->setEnabled(0);
        ui->bDisc->setEnabled(0);

        client->disconnectMDB();
        return false;
    }
}

void CalibrationPage::on_bDisc_clicked()
{
    if(client->isConnected == true){
        client->disconnectMDB();
        ui->bCalib->setEnabled(1);
        ui->bDisc->setEnabled(0);
        ui->bCalibCP->setEnabled(0);
        ui->bCalibCP_Minus->setEnabled(0);
        ui->bCalibTemp1->setEnabled(0);
        ui->bCalibTemp2->setEnabled(0);
        ui->bMeausre->setEnabled(0);
        ui->bTemp2_setP2->setEnabled(0);
        ui->btemp1_setPkt2->setEnabled(0);
        ui->btemp1_setPMin->setEnabled(0);
        ui->btemp1_setPMax->setEnabled(0);
        ui->btemp1_setP1_2->setEnabled(0);
        ui->btemp1_setP2_2->setEnabled(0);
        ui->btemp1_setP1->setEnabled(0);
        ui->btemp2_setP1->setEnabled(0);
        ui->labInfo->setText("Rozłączono!");
    }
    else
    {
        ui->labInfo->setText("Port jest rozłączony!");
        ui->bCalib->setEnabled(1); //przycisk połączenia
        ui->bDisc->setEnabled(0);
    }
}

void CalibrationPage::on_bCalibTemp1_clicked()
{
    int p1 = ui->temp1_pkt1_min->text().toInt();
    int p2 = ui->temp1_pkt1_max->text().toInt();

    kalibracjaWartosci(p1,p2);
    ui->btemp1_setP1->setEnabled(1);
    ui->bCalibTemp1->setEnabled(0);
    ui->btemp1_setPMin->setEnabled(1);
    ui->btemp2_setP1->setEnabled(1);

    sendData("40"); //calculate temp1
    sendData("100");
    sendData("101");
    ui->labInfo->setText("Skalibrowano");
}

void CalibrationPage::on_bCalibTemp2_clicked()
{
    int p1 = ui->temp2_pkt1_min->text().toInt();
    int p2 = ui->temp2_pkt1_max->text().toInt();
    ui->btemp1_setP1->setEnabled(1);
    ui->bCalibTemp1->setEnabled(0);
    ui->btemp1_setPMin->setEnabled(1);
    ui->btemp2_setP1->setEnabled(1);

    kalibracjaWartosci(p1,p2);
    ui->bCalibTemp2->setEnabled(0);
    ui->btemp2_setP1->setEnabled(1);
    sendData("50");
    sendData("100");
    ui->labInfo->setText("Skalibrowano");
}

void CalibrationPage::on_bCalibCP_clicked()
{

    int p1 = ui->cp_p1_min->text().toInt();
    int p2 = ui->cp_p1_max->text().toInt();

    sendData("20");
    kalibracjaWartosci(p1,p2);
    ui->btemp1_setPMin->setEnabled(1);
    ui->btemp1_setPMax->setEnabled(0);
    ui->btemp1_setP2_2->setEnabled(0);
    ui->bCalibCP->setEnabled(0);
    ui->btemp1_setP1->setEnabled(1);
    ui->btemp2_setP1->setEnabled(1);
    sendData("100");
    ui->labInfo->setText("Skalibrowano");
}

void CalibrationPage::on_bCalibCP_Minus_clicked()
{
    int p1 = ui->cp_p2_min->text().toInt();
    int p2 = ui->cp_p2_max->text().toInt();

    kalibracjaWartosci(p1,p2);
    sendData("10");
    ui->btemp1_setPMax->setEnabled(1);
    ui->btemp1_setP1_2->setEnabled(0);
    ui->bCalibCP_Minus->setEnabled(0);
    ui->btemp1_setP1->setEnabled(0);
    sendData("100");
    ui->labInfo->setText("Skalibrowano");
}


void CalibrationPage::on_bTemp2_setP2_clicked()
{
    sendData("52");
    ui->bCalibTemp2->setEnabled(1);
    ui->bTemp2_setP2->setEnabled(0);
    createInfoForm();
    ui->labInfo->setText("Ustawiono punkt");
}

void CalibrationPage::on_btemp1_setP2_clicked()
{
    sendData("42");
    float z = 51.2*(ui->temp1_pkt1_max->text().toInt()) - 4096;
    qDebug() << z << "\n";
    ui->bCalibTemp1->setEnabled(1);
}

void CalibrationPage::on_btemp1_setPMin_clicked()
{

    ui->btemp1_setP1_2->setEnabled(1);
    ui->btemp1_setPMax->setEnabled(0);
    ui->btemp1_setPMin->setEnabled(0);
    ui->btemp1_setP1->setEnabled(0);
    ui->btemp2_setP1->setEnabled(0);
    ui->btemp1_setP1->setEnabled(0);
    createInfoForm();
    sendData("11");
    sendData("100");
//    sendData("101");
}

void CalibrationPage::on_btemp1_setPMax_clicked()
{
    ui->btemp1_setP2_2->setEnabled(1);
    ui->btemp1_setPMax->setEnabled(1);
    ui->btemp1_setPMax->setEnabled(0);
    ui->btemp1_setP1->setEnabled(0);
    ui->btemp2_setP1->setEnabled(0);
    createInfoForm();
    sendData("21");
    sendData("100");
//    sendData("101");
}

void CalibrationPage::on_btemp1_setPkt2_clicked()
{
    ui->btemp1_setPkt2->setEnabled(0);
    ui->bCalibTemp1->setEnabled(1);
    createInfoForm();
    sendData("42");
    sendData("100");
//    sendData("101");
}

void CalibrationPage::timerTick()
{
    t = eTimer->elapsed();
    if(t >= 5000){
        eTimer->restart();
        timer->stop();
    }
}

void CalibrationPage::createInfoForm()
{
    infoForm = new InfoForm(this);  //utwórz okno informacyjne
    eTimer = new QElapsedTimer;
    timer = new QTimer(this);   //timer
    timer->setInterval(3000);
    infoForm->setGeometry(100,100,700,500);
    infoForm->show();           //pokaz okno
    connect(timer,SIGNAL(timeout()),this, SLOT(timerTick()));
    connect(timer,SIGNAL(timeout()),infoForm,SLOT(hide())); //zamknij okno
    timer->start();
    eTimer->start();
}

void CalibrationPage::measureValues()
{
    uint16_t odp;
    if(ui->checkZiarno->checkState() == Qt::Checked)
     {
         sendData("101");
         odp = getRegisters(77);
         ui->lcdTemp1->display(odp);
         ui->labDispT1->setText("Wartość ziarna rezystancji 1:");
         if(odp == 0)
             ui->labInfo->setText("Błąd kalibracji");
     }
     else
     {
        sendData("101");
         odp = getRegisters(77);            //odczyt danych

         float value = (odp+4096)/51.2;
         ui->labDispT1->setText("Wartość rezystancji [Ω]: ");
         ui->lcdTemp1->display(value);
     }


    if(ui->checkZiarno_2->checkState() == Qt::Checked)
    {
        odp = getRegisters(78);
        ui->lcd_Temp2->display(odp);
        ui->labDispT2->setText("Wartość ziarna rezystancji 2:");
    }
    else{
        odp = getRegisters(78);

        float value = (odp+4096)/5.12;
        ui->lcd_Temp2->display(value);
        ui->labDispT2->setText("Wartość rezystancji [Ω]: ");
    }



    if(ui->checkZiarno_3->checkState() == Qt::Checked)
     {
        odp = getRegisters(76);
        ui->lcdCP->display(odp);
        ui->lcdCP_2->display(odp);
        ui->labDispCP1->setText("Ziarno napięcia dodatniego ");
        ui->labDispCP2->setText("Ziarno napięcia ujemnego ");
    }
    else
    {
        odp = getRegisters(76);
        float value = (odp)/341.25;
        ui->lcdCP->display(value);
        ui->lcdCP_2->display((-1)*value);
        ui->labDispCP1->setText("Wartość napięcia dodatniego  CP+ [V] ");
        ui->labDispCP2->setText("Wartość napięcia ujemnego CP- [V]");
    }

    if(ifDisconnected == true)
        measureTimer->stop();
}

void CalibrationPage::on_bMeausre_clicked()
{
    measureTimer = new QTimer(this);
    measureTimer->setInterval(1000);
    measureTimer->start();  //wlącz zegar
    ifDisconnected = false;
    connect(measureTimer,SIGNAL(timeout()),this,SLOT(measureValues()));
    ui->bMeausre->setEnabled(0);
    ui->bMeausre_2->setEnabled(1);
    ui->bDisc->setEnabled(0);
    sendData("101");
}

void CalibrationPage::on_bMeausre_2_clicked()
{
    ifDisconnected = true;
    ui->bMeausre->setEnabled(1);
    ui->bMeausre_2->setEnabled(0);
    ui->bDisc->setEnabled(1);
}

//---------------------wykresy-------------------------
void CalibrationPage::on_bCreatePlot_clicked()
{
    int typ = ui->wyborWYkresu->currentIndex();

    QVector<double> x(21),y(21);

    int zakres_min, zakres_max;

    if(typ == 0)
    {
        zakres_min = ui->temp1_pkt1_min->text().toInt();
        zakres_max = ui->temp1_pkt1_max->text().toInt();

        for(int i=0; i<21; i++){
            x[i] = 4*i + 80;                //wyznaczanie funkcji temp. 1
            y[i] = 51.2*x[i] - 4096;
        }
    }
    else if(typ==1)
    {
        zakres_min = ui->temp2_pkt1_min->text().toInt();
        zakres_max = ui->temp2_pkt1_max->text().toInt();

        for(int i=0; i<21; i++){
            x[i] = 400*i + 800;
            y[i] = 5.12*x[i] - 4096;            //funkcja temp. 2
        }
    }
    else if(typ == 2)
    {
        zakres_min = ui->cp_p1_min->text().toInt();
        zakres_max = ui->cp_p2_max->text().toInt();

        for(int i = zakres_max; i>0; --i)
        {
            x[i] = (-1)*i;
            y[i] = -341*x[i];               //funkcja cp+ i cp-
        }

        for(int i = 0; i<zakres_max; ++i)
        {
            x[i] = i;
            y[i] = 341*x[i];
        }
    }

    if(typ != 2)
    {
        ui->wykres->addGraph(); //dodaj wykres
        ui->wykres->graph(0)->setData(x,y); //ustaw dane
        ui->wykres->xAxis->setLabel("Rezystancja [Ω]");
        ui->wykres->yAxis->setLabel("Ziarno");
        ui->wykres->yAxis->setRange(0,4096);
        ui->wykres->xAxis->setRange(zakres_min,zakres_max);
        ui->wykres->replot();
     }
     else{
        ui->wykres->addGraph(); //dodaj wykres
        ui->wykres->graph(0)->setData(x,y); //ustaw dane
        ui->wykres->xAxis->setLabel("Napięcie [V]");
        ui->wykres->yAxis->setLabel("Ziarno");
        ui->wykres->yAxis->setRange(0,4096);
        ui->wykres->xAxis->setRange(zakres_min,zakres_max);
        ui->wykres->replot();
    }
}
