#include "ConsoleWidget.h"
#include "ui_ConsoleWidget.h"
#include <QDateTime>
#include <QScrollBar>
#include <QDebug>

#define COLOR_DEFAULT "#000000"
#define COLOR_INFO "#0000ff"    // niebieski
#define COLOR_WARN "#ff7911"    // zolty
#define COLOR_ERROR "#ff0000"

ConsoleWidget::ConsoleWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ConsoleWidget)
{
    ui->setupUi(this);
}

ConsoleWidget::~ConsoleWidget()
{
    delete ui;
}

void ConsoleWidget::addMsg(QString msg, int icon)
{
    QString color;
    switch((QMessageBox::Icon) icon){
    case QMessageBox::Information:
        color = COLOR_INFO;
        break;
    case QMessageBox::Warning:
        color = COLOR_WARN;
        break;
    case QMessageBox::Critical:
        color = COLOR_ERROR;
        break;
    default:
        color = COLOR_DEFAULT;
    }

    QString fullMsg = "<p style=\"margin: 0; color: " + color + ";\">";
    fullMsg += "[" + QDateTime::currentDateTime().toString("HH:mm:ss.zzz] - ");
    fullMsg += msg;
    fullMsg += "</p>";

    ui->textEdit->append(fullMsg);
    ui->textEdit->verticalScrollBar()->setValue( ui->textEdit->verticalScrollBar()->maximum() );
}

void ConsoleWidget::on_clearButton_clicked()
{
    ui->textEdit->clear();
}

void ConsoleWidget::on_closeButton_clicked()
{
    emit closeButtonClicked();
}
