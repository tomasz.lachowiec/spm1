#ifndef CONSOLEWIDGET_H
#define CONSOLEWIDGET_H

#include <QWidget>
#include <QMessageBox>

namespace Ui {
class ConsoleWidget;
}

class ConsoleWidget : public QWidget
{
    Q_OBJECT

public:
    explicit ConsoleWidget(QWidget *parent = 0);
    ~ConsoleWidget();

signals:
    void closeButtonClicked();

public slots:
    void addMsg(QString msg, int icon = QMessageBox::NoIcon);

    void on_clearButton_clicked();
    void on_closeButton_clicked();

private:
    Ui::ConsoleWidget *ui;
};

#endif // CONSOLEWIDGET_H
