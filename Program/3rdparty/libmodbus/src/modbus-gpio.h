#ifndef MODBUS_GPIO_H
#define MODBUS_GPIO_H

#ifdef __cplusplus
extern "C" {
#endif

int gpio_export(unsigned int gpio);
int gpio_unexport(unsigned int gpio);
int gpio_set_dir(unsigned int gpio, unsigned int out_flag);
int gpio_set_value(unsigned int gpio, unsigned int value);
int gpio_get_value(unsigned int gpio, unsigned int *value);

#ifdef __cplusplus
}
#endif

#endif
