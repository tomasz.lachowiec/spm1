# Polaczenie libmodbus i qextserialport

QT       -= gui

TARGET = modbus
TEMPLATE = lib
#VERSION = 0.1.0
win32:CONFIG += dll

SOURCES +=  qextserialport/qextserialport.cpp	\
    libmodbus/src/modbus.c \
    libmodbus/src/modbus-data.c \
    libmodbus/src/modbus-rtu.c \
    libmodbus/src/modbus-tcp.c \
    libmodbus/src/modbus-gpio.c
HEADERS +=   qextserialport/qextserialport.h \
    qextserialport/qextserialenumerator.h \
    libmodbus/src/modbus.h \
    libmodbus/src/modbus-gpio.h

INCLUDEPATH += libmodbus qextserialport

unix{
    SOURCES += qextserialport/posix_qextserialport.cpp	\
            qextserialport/qextserialenumerator_unix.cpp
}
unix{
    DEFINES += _TTY_POSIX_
}
win32{
    SOURCES += qextserialport/win_qextserialport.cpp \
                qextserialport/qextserialenumerator_win.cpp
}
win32{
    DEFINES += _TTY_WIN_  WINVER=0x0501
}
win32{
    LIBS += -lsetupapi -lwsock32
    LIBS += -lws2_32
}
