//-----------------------------------------------------------------------------
//! @class ConfigManager
//!
//! @par Copyrights:
//!	(c) 2009-2014 by DESTECH
//!
//! @author Dominik Swałdek
//!
//! @par Desc:
//!	Obiekt konfiguracji (zarowno roboczej jak i edycji)
//!
//! @version 1.0 - 27.25.2014 - Initial release.
//!
//-----------------------------------------------------------------------------

#include "ConfigObj.h"
#include <QSettings>
#include <QFile>
#include <QDebug>

//ConfigObj::ConfigObj(QObject *parent) : QObject(parent)
ConfigObj::ConfigObj()
{
//    this->createDefaultPagesDataSets();
//    this->createDefaultScreensCfg();
}

/**
 * Odczyt konfiguracji z pliku
 * @param filePath Pelna sciezka do pliku
 * @return enum ze statusem operacji
*/
int ConfigObj::loadFromFile(QString filePath)
{
    if(! QFile::exists(filePath))
        return NO_FILE;

    QSettings settings(filePath, QSettings::IniFormat);
    int status = settings.status();
    if(status == QSettings::FormatError )
        return FILE_CORRUPTED;

    // naglowek
    settings.beginGroup("head");
   if( !settings.contains("CFG_VER_MAJOR") || !settings.contains("CFG_VER_MAINOR"))
        return FILE_CORRUPTED;
    int cfgMajorVer = settings.value("CFG_VER_MAJOR").toInt();
    int cfgMinorVer = settings.value("CFG_VER_MAINOR").toInt();
    // sprawdzenie wersji
    qDebug() << "Readed cfg file ver: " << cfgMajorVer << "." << cfgMinorVer;
    settings.endGroup();
    if(cfgMajorVer != CFG_VER_MAJOR)
        return FILE_INVALID_VER;

    int missedParameters = 0;

    //ogolne
     settings.beginGroup("device");
    if(settings.contains("deviceID_Cfg")) {
        generalSettings.deviceID_Cfg = settings.value("deviceID_Cfg").toString();
    } else {missedParameters++;}
    if(settings.contains("deviceDesc_Cfg")) {
        generalSettings.deviceDesc_Cfg = settings.value("deviceDesc_Cfg").toString();
    } else {missedParameters++;}
    if(settings.contains("lang_Cfg")) {
        generalSettings.lang_Cfg = settings.value("lang_Cfg").toBool();
    } else {missedParameters++;}
    settings.endGroup();

    //konfiguracja programu flash
    settings.beginGroup("firmware");
    if(settings.contains("versionFirmware_Cfg")) {
        generalSettings.versionFirmware_Cfg = settings.value("versionFirmware_Cfg").toString();
    } else {missedParameters++;}
    if(settings.contains("filePathFirmware_Cfg")) {
        generalSettings.filePathFirmware_Cfg = settings.value("filePathFirmware_Cfg").toString();
    } else {missedParameters++;}
      settings.endGroup();

    //konfiguracja testowania modułow
    settings.beginGroup("module");
    if(settings.contains("enableModule_1_Cfg")) {
        generalSettings.enableModule_1_Cfg = settings.value("enableModule_1_Cfg").toBool();
    } else {missedParameters++;}
    if(settings.contains("enableModule_1_Cfg")) {
        generalSettings.enableModule_2_Cfg = settings.value("enableModule_2_Cfg").toBool();
    } else {missedParameters++;}
    if(settings.contains("enableModule_1_Cfg")) {
        generalSettings.enableModule_3_Cfg = settings.value("enableModule_3_Cfg").toBool();
    } else {missedParameters++;}

    if(settings.contains("calibrateModeModule_1_Cfg")) {
        generalSettings.calibrateModeModule_1_Cfg = settings.value("calibrateModeModule_1_Cfg").toInt();
    } else {missedParameters++;}
    if(settings.contains("calibrateModeModule_1_Cfg")) {
        generalSettings.calibrateModeModule_1_Cfg = settings.value("calibrateModeModule_1_Cfg").toInt();
    } else {missedParameters++;}
    if(settings.contains("calibrateModeModule_1_Cfg")) {
        generalSettings.calibrateModeModule_1_Cfg = settings.value("calibrateModeModule_1_Cfg").toInt();
    } else {missedParameters++;}

    if(settings.contains("calibrateResistancePoint_1_Module_1_Sensor_1_Cfg")) {
        generalSettings.calibrateResistancePoint_1_Module_1_Sensor_1_Cfg = settings.value("calibrateResistancePoint_1_Module_1_Sensor_1_Cfg").toFloat();
    } else {missedParameters++;}
    if(settings.contains("calibrateResistancePoint_2_Module_1_Sensor_1_Cfg")) {
        generalSettings.calibrateResistancePoint_2_Module_1_Sensor_1_Cfg = settings.value("calibrateResistancePoint_2_Module_1_Sensor_1_Cfg").toFloat();
    } else {missedParameters++;}
    if(settings.contains("calibrateResistancePoint_1_Module_1_Sensor_2_Cfg")) {
        generalSettings.calibrateResistancePoint_1_Module_1_Sensor_2_Cfg = settings.value("calibrateResistancePoint_1_Module_1_Sensor_2_Cfg").toFloat();
    } else {missedParameters++;}
    if(settings.contains("calibrateResistancePoint_2_Module_1_Sensor_2_Cfg")) {
        generalSettings.calibrateResistancePoint_2_Module_1_Sensor_2_Cfg = settings.value("calibrateResistancePoint_2_Module_1_Sensor_2_Cfg").toFloat();
    } else {missedParameters++;}

    if(settings.contains("calibrateResistancePoint_1_Module_2_Sensor_1_Cfg")) {
        generalSettings.calibrateResistancePoint_1_Module_2_Sensor_1_Cfg = settings.value("calibrateResistancePoint_1_Module_2_Sensor_1_Cfg").toFloat();
    } else {missedParameters++;}
    if(settings.contains("calibrateResistancePoint_2_Module_2_Sensor_1_Cfg")) {
        generalSettings.calibrateResistancePoint_2_Module_2_Sensor_1_Cfg = settings.value("calibrateResistancePoint_2_Module_2_Sensor_1_Cfg").toFloat();
    } else {missedParameters++;}
    if(settings.contains("calibrateResistancePoint_1_Module_2_Sensor_2_Cfg")) {
        generalSettings.calibrateResistancePoint_1_Module_2_Sensor_2_Cfg = settings.value("calibrateResistancePoint_1_Module_2_Sensor_2_Cfg").toFloat();
    } else {missedParameters++;}
    if(settings.contains("calibrateResistancePoint_2_Module_2_Sensor_2_Cfg")) {
        generalSettings.calibrateResistancePoint_2_Module_2_Sensor_2_Cfg = settings.value("calibrateResistancePoint_2_Module_2_Sensor_2_Cfg").toFloat();
    } else {missedParameters++;}

    if(settings.contains("calibrateResistancePoint_1_Module_3_Sensor_1_Cfg")) {
        generalSettings.calibrateResistancePoint_1_Module_3_Sensor_1_Cfg = settings.value("calibrateResistancePoint_1_Module_3_Sensor_1_Cfg").toFloat();
    } else {missedParameters++;}
    if(settings.contains("calibrateResistancePoint_2_Module_3_Sensor_1_Cfg")) {
        generalSettings.calibrateResistancePoint_2_Module_3_Sensor_1_Cfg = settings.value("calibrateResistancePoint_2_Module_3_Sensor_1_Cfg").toFloat();
    } else {missedParameters++;}
    if(settings.contains("calibrateResistancePoint_1_Module_3_Sensor_2_Cfg")) {
        generalSettings.calibrateResistancePoint_1_Module_3_Sensor_2_Cfg = settings.value("calibrateResistancePoint_1_Module_3_Sensor_2_Cfg").toFloat();
    } else {missedParameters++;}
    if(settings.contains("calibrateResistancePoint_2_Module_3_Sensor_2_Cfg")) {
        generalSettings.calibrateResistancePoint_2_Module_3_Sensor_2_Cfg = settings.value("calibrateResistancePoint_2_Module_3_Sensor_2_Cfg").toFloat();
    } else {missedParameters++;}


    if(settings.contains("calibrateTemperaturePoint_1_Module_1_Sensor_2_Cfg")) {
        generalSettings.calibrateTemperaturePoint_1_Module_1_Sensor_2_Cfg = settings.value("calibrateTemperaturePoint_1_Module_1_Sensor_2_Cfg").toFloat();
    } else {missedParameters++;}
    if(settings.contains("calibrateTemperaturePoint_2_Module_1_Sensor_2_Cfg")) {
        generalSettings.calibrateTemperaturePoint_2_Module_1_Sensor_2_Cfg = settings.value("calibrateTemperaturePoint_2_Module_1_Sensor_2_Cfg").toFloat();
    } else {missedParameters++;}

    if(settings.contains("calibrateTemperaturePoint_1_Module_2_Sensor_2_Cfg")) {
        generalSettings.calibrateTemperaturePoint_1_Module_2_Sensor_2_Cfg = settings.value("calibrateTemperaturePoint_1_Module_2_Sensor_2_Cfg").toFloat();
    } else {missedParameters++;}
    if(settings.contains("calibrateTemperaturePoint_2_Module_2_Sensor_2_Cfg")) {
        generalSettings.calibrateTemperaturePoint_2_Module_2_Sensor_2_Cfg = settings.value("calibrateTemperaturePoint_2_Module_2_Sensor_2_Cfg").toFloat();
    } else {missedParameters++;}

    if(settings.contains("calibrateTemperaturePoint_1_Module_3_Sensor_2_Cfg")) {
        generalSettings.calibrateTemperaturePoint_1_Module_3_Sensor_2_Cfg = settings.value("calibrateTemperaturePoint_1_Module_3_Sensor_2_Cfg").toFloat();
    } else {missedParameters++;}
    if(settings.contains("calibrateTemperaturePoint_2_Module_3_Sensor_2_Cfg")) {
        generalSettings.calibrateTemperaturePoint_2_Module_3_Sensor_2_Cfg = settings.value("calibrateTemperaturePoint_2_Module_3_Sensor_2_Cfg").toFloat();
    } else {missedParameters++;}


    if(settings.contains("enableTestModule_1_Cfg")) {
        generalSettings.enableTestModule_1_Cfg = settings.value("enableTestModule_1_Cfg").toBool();
    } else {missedParameters++;}
    if(settings.contains("enableTestModule_2_Cfg")) {
        generalSettings.enableTestModule_2_Cfg = settings.value("enableTestModule_2_Cfg").toBool();
    } else {missedParameters++;}
    if(settings.contains("enableTestModule_3_Cfg")) {
        generalSettings.enableTestModule_3_Cfg = settings.value("enableTestModule_3_Cfg").toBool();
    } else {missedParameters++;}


    if(settings.contains("testResistancePointVal_1_Module_1_Sensor_1_Cfg")) {
        generalSettings.testResistancePointVal_1_Module_1_Sensor_1_Cfg = settings.value("testResistancePointVal_1_Module_1_Sensor_1_Cfg").toFloat();
    } else {missedParameters++;}
    if(settings.contains("testResistancePointVal_2_Module_1_Sensor_1_Cfg")) {
        generalSettings.testResistancePointVal_2_Module_1_Sensor_1_Cfg = settings.value("testResistancePointVal_2_Module_1_Sensor_1_Cfg").toFloat();
    } else {missedParameters++;}
    if(settings.contains("testResistancePointVal_1_Module_1_Sensor_2_Cfg")) {
        generalSettings.testResistancePointVal_1_Module_1_Sensor_2_Cfg = settings.value("testResistancePointVal_1_Module_1_Sensor_2_Cfg").toFloat();
    } else {missedParameters++;}
    if(settings.contains("testResistancePointVal_2_Module_1_Sensor_2_Cfg")) {
        generalSettings.testResistancePointVal_2_Module_1_Sensor_2_Cfg = settings.value("testResistancePointVal_2_Module_1_Sensor_2_Cfg").toFloat();
    } else {missedParameters++;}

    if(settings.contains("testResistancePointVal_1_Module_2_Sensor_1_Cfg")) {
        generalSettings.testResistancePointVal_1_Module_2_Sensor_1_Cfg = settings.value("testResistancePointVal_1_Module_2_Sensor_1_Cfg").toFloat();
    } else {missedParameters++;}
    if(settings.contains("testResistancePointVal_2_Module_2_Sensor_1_Cfg")) {
        generalSettings.testResistancePointVal_2_Module_2_Sensor_1_Cfg = settings.value("testResistancePointVal_2_Module_2_Sensor_1_Cfg").toFloat();
    } else {missedParameters++;}
    if(settings.contains("testResistancePointVal_1_Module_2_Sensor_2_Cfg")) {
        generalSettings.testResistancePointVal_1_Module_2_Sensor_2_Cfg = settings.value("testResistancePointVal_1_Module_2_Sensor_2_Cfg").toFloat();
    } else {missedParameters++;}
    if(settings.contains("testResistancePointVal_2_Module_2_Sensor_2_Cfg")) {
        generalSettings.testResistancePointVal_2_Module_2_Sensor_2_Cfg = settings.value("testResistancePointVal_2_Module_2_Sensor_2_Cfg").toFloat();
    } else {missedParameters++;}

    if(settings.contains("testResistancePointVal_1_Module_3_Sensor_1_Cfg")) {
        generalSettings.testResistancePointVal_1_Module_3_Sensor_1_Cfg = settings.value("testResistancePointVal_1_Module_3_Sensor_1_Cfg").toFloat();
    } else {missedParameters++;}
    if(settings.contains("testResistancePointVal_2_Module_3_Sensor_1_Cfg")) {
        generalSettings.testResistancePointVal_2_Module_3_Sensor_1_Cfg = settings.value("testResistancePointVal_2_Module_3_Sensor_1_Cfg").toFloat();
    } else {missedParameters++;}

    if(settings.contains("testResistancePointVal_1_Module_3_Sensor_2_Cfg")) {
        generalSettings.testResistancePointVal_1_Module_3_Sensor_2_Cfg = settings.value("testResistancePointVal_1_Module_3_Sensor_2_Cfg").toFloat();
    } else {missedParameters++;}
    if(settings.contains("testResistancePointVal_2_Module_3_Sensor_2_Cfg")) {
        generalSettings.testResistancePointVal_2_Module_3_Sensor_2_Cfg = settings.value("testResistancePointVal_2_Module_3_Sensor_2_Cfg").toFloat();
    } else {missedParameters++;}
    settings.endGroup();

    if(missedParameters > 0)
        qDebug() << "!!!!!!!!!!! Config file missed parameters: " << missedParameters << " !!!!!!!!!!!!";

    return OK;
}

/**
 * Zapis konfiguracji do pliku
 * @param filePath Pelna sciezka gdzie zapisac plik
 * @return enum ze statusem operacji
*/
int ConfigObj::save2File(QString filePath)
{
    QSettings settings(filePath, QSettings::IniFormat);

    if(settings.isWritable()){

        // naglowek
        settings.beginGroup("head");
        settings.setValue("CFG_VER_MAJOR", CFG_VER_MAJOR);
        settings.setValue("CFG_VER_MAINOR", CFG_VER_MAINOR);
        settings.endGroup();

        //urzadzenie
        settings.beginGroup("device");
        settings.setValue("deviceID_Cfg",           generalSettings.deviceID_Cfg);
        settings.setValue("deviceDesc_Cfg",          generalSettings.deviceDesc_Cfg);
        settings.setValue("lang_Cfg",               generalSettings.lang_Cfg);
        settings.endGroup();

        //konfiguracja programu flash
        settings.beginGroup("firmware");
        settings.setValue("versionFirmware_Cfg",      generalSettings.versionFirmware_Cfg);
        settings.setValue("filePathFirmware_Cfg",      generalSettings.filePathFirmware_Cfg);
        settings.endGroup();

        //konfiguracja testowania modułow
        settings.beginGroup("module");

        settings.setValue("enableModule_1_Cfg",      generalSettings.enableModule_1_Cfg);
        settings.setValue("enableModule_2_Cfg",      generalSettings.enableModule_2_Cfg);
        settings.setValue("enableModule_3_Cfg",      generalSettings.enableModule_3_Cfg);


        settings.setValue("calibrateModeModule_1_Cfg",      generalSettings.calibrateModeModule_1_Cfg);
        settings.setValue("calibrateModeModule_2_Cfg",      generalSettings.calibrateModeModule_2_Cfg);
        settings.setValue("calibrateModeModule_3_Cfg",      generalSettings.calibrateModeModule_3_Cfg);

        settings.setValue("calibrateResistancePoint_1_Module_1_Sensor_1_Cfg",      generalSettings.calibrateResistancePoint_1_Module_1_Sensor_1_Cfg);
        settings.setValue("calibrateResistancePoint_2_Module_1_Sensor_1_Cfg",      generalSettings.calibrateResistancePoint_2_Module_1_Sensor_1_Cfg);
        settings.setValue("calibrateResistancePoint_1_Module_1_Sensor_2_Cfg",      generalSettings.calibrateResistancePoint_1_Module_1_Sensor_2_Cfg);
        settings.setValue("calibrateResistancePoint_2_Module_1_Sensor_2_Cfg",      generalSettings.calibrateResistancePoint_2_Module_1_Sensor_2_Cfg);

        settings.setValue("calibrateResistancePoint_1_Module_2_Sensor_1_Cfg",      generalSettings.calibrateResistancePoint_1_Module_2_Sensor_1_Cfg);
        settings.setValue("calibrateResistancePoint_2_Module_2_Sensor_1_Cfg",      generalSettings.calibrateResistancePoint_2_Module_2_Sensor_1_Cfg);
        settings.setValue("calibrateResistancePoint_1_Module_2_Sensor_2_Cfg",      generalSettings.calibrateResistancePoint_1_Module_2_Sensor_2_Cfg);
        settings.setValue("calibrateResistancePoint_2_Module_2_Sensor_2_Cfg",      generalSettings.calibrateResistancePoint_2_Module_2_Sensor_2_Cfg);

        settings.setValue("calibrateResistancePoint_1_Module_3_Sensor_1_Cfg",      generalSettings.calibrateResistancePoint_1_Module_3_Sensor_1_Cfg);
        settings.setValue("calibrateResistancePoint_2_Module_3_Sensor_1_Cfg",      generalSettings.calibrateResistancePoint_2_Module_3_Sensor_1_Cfg);
        settings.setValue("calibrateResistancePoint_1_Module_3_Sensor_2_Cfg",      generalSettings.calibrateResistancePoint_1_Module_3_Sensor_2_Cfg);
        settings.setValue("calibrateResistancePoint_2_Module_3_Sensor_2_Cfg",      generalSettings.calibrateResistancePoint_2_Module_3_Sensor_2_Cfg);

        settings.setValue("calibrateTemperaturePoint_1_Module_1_Sensor_1_Cfg",     generalSettings.calibrateTemperaturePoint_1_Module_1_Sensor_1_Cfg);
        settings.setValue("calibrateTemperaturePoint_2_Module_1_Sensor_1_Cfg",     generalSettings.calibrateTemperaturePoint_2_Module_1_Sensor_1_Cfg);
        settings.setValue("calibrateTemperaturePoint_1_Module_1_Sensor_2_Cfg",     generalSettings.calibrateTemperaturePoint_1_Module_1_Sensor_2_Cfg);
        settings.setValue("calibrateTemperaturePoint_2_Module_1_Sensor_2_Cfg",     generalSettings.calibrateTemperaturePoint_2_Module_1_Sensor_2_Cfg);

        settings.setValue("calibrateTemperaturePoint_1_Module_2_Sensor_1_Cfg",     generalSettings.calibrateTemperaturePoint_1_Module_2_Sensor_1_Cfg);
        settings.setValue("calibrateTemperaturePoint_2_Module_2_Sensor_1_Cfg",     generalSettings.calibrateTemperaturePoint_2_Module_2_Sensor_1_Cfg);
        settings.setValue("calibrateTemperaturePoint_1_Module_2_Sensor_2_Cfg",     generalSettings.calibrateTemperaturePoint_1_Module_2_Sensor_2_Cfg);
        settings.setValue("calibrateTemperaturePoint_2_Module_2_Sensor_2_Cfg",     generalSettings.calibrateTemperaturePoint_2_Module_2_Sensor_2_Cfg);

        settings.setValue("calibrateTemperaturePoint_1_Module_3_Sensor_1_Cfg",     generalSettings.calibrateTemperaturePoint_1_Module_3_Sensor_1_Cfg);
        settings.setValue("calibrateTemperaturePoint_2_Module_3_Sensor_1_Cfg",      generalSettings.calibrateTemperaturePoint_2_Module_3_Sensor_1_Cfg);
        settings.setValue("calibrateTemperaturePoint_1_Module_3_Sensor_2_Cfg",     generalSettings.calibrateTemperaturePoint_1_Module_3_Sensor_2_Cfg);
        settings.setValue("calibrateTemperaturePoint_2_Module_3_Sensor_2_Cfg",      generalSettings.calibrateTemperaturePoint_2_Module_3_Sensor_2_Cfg);

        settings.setValue("enableTestModule_1_Cfg",      generalSettings.enableTestModule_1_Cfg);
        settings.setValue("enableTestModule_2_Cfg",      generalSettings.enableTestModule_2_Cfg);
        settings.setValue("enableTestModule_3_Cfg",      generalSettings.enableTestModule_3_Cfg);

        settings.setValue("testResistancePointVal_1_Module_1_Sensor_1_Cfg",     generalSettings.testResistancePointVal_1_Module_1_Sensor_1_Cfg);
        settings.setValue("testResistancePointVal_2_Module_1_Sensor_1_Cfg",     generalSettings.testResistancePointVal_2_Module_1_Sensor_1_Cfg);
        settings.setValue("testResistancePointVal_1_Module_1_Sensor_2_Cfg",     generalSettings.testResistancePointVal_1_Module_1_Sensor_2_Cfg);
        settings.setValue("testResistancePointVal_2_Module_1_Sensor_2_Cfg",     generalSettings.testResistancePointVal_2_Module_1_Sensor_2_Cfg);

        settings.setValue("testResistancePointVal_1_Module_2_Sensor_1_Cfg",     generalSettings.testResistancePointVal_1_Module_2_Sensor_1_Cfg);
        settings.setValue("testResistancePointVal_2_Module_2_Sensor_1_Cfg",     generalSettings.testResistancePointVal_2_Module_2_Sensor_1_Cfg);
        settings.setValue("testResistancePointVal_1_Module_2_Sensor_2_Cfg",     generalSettings.testResistancePointVal_1_Module_2_Sensor_2_Cfg);
        settings.setValue("testResistancePointVal_2_Module_2_Sensor_2_Cfg",     generalSettings.testResistancePointVal_2_Module_2_Sensor_2_Cfg);

        settings.setValue("testResistancePointVal_1_Module_3_Sensor_1_Cfg",     generalSettings.testResistancePointVal_1_Module_3_Sensor_1_Cfg);
        settings.setValue("testResistancePointVal_2_Module_3_Sensor_1_Cfg",     generalSettings.testResistancePointVal_2_Module_3_Sensor_1_Cfg);
        settings.setValue("testResistancePointVal_1_Module_3_Sensor_2_Cfg",     generalSettings.testResistancePointVal_1_Module_3_Sensor_2_Cfg);
        settings.setValue("testResistancePointVal_2_Module_3_Sensor_2_Cfg",     generalSettings.testResistancePointVal_2_Module_3_Sensor_2_Cfg);

        settings.endGroup();

        settings.sync();
        return OK;
    }

    return UNABLE_2_WRITE;
}


/////** Utworzenie danych defaults */
//void ConfigObj::createDefaultDataSets()
//{






//}

