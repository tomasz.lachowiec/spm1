#ifndef CONFIGSOFTWARE_H
#define CONFIGSOFTWARE_H

#include <QObject>

#include "Modbus/ModbusClient.h"
#include "inc/def.h"
//#include "ConfigObj.h"

//class ConfigSoftware : public QObject
class ConfigSoftware
{

public:

    enum{
        CFG_VER_MAJOR = 1,
        CFG_VER_MAINOR = 1
    };

    enum{
        OK,
        UNABLE_2_WRITE, // brak uprawnien do zapisu
        NO_FILE,        // brak pliku w podanej sciezce
        FILE_CORRUPTED, // uszkodzony lub niewlasciwy plik
        FILE_INVALID_VER // nieprawidlowa wersja pliku (roznica w CFG_VER_MAJOR)
    };

    //explicit ConfigSoftware(QObject *parent = 0);
    explicit ConfigSoftware();
    ~ConfigSoftware();

    int loadFromFile(QString filePath);
    int save2File(QString filePath);
    void createDefaultDataSets(void);



    //--------------------------------------------------------------------
    // Klasy podgrup konfiguracji
    //--------------------------------------------------------------------

    class ModuleRGB{
    public:
        ModuleRGB() : enable(false), modbusAddres(1) {}

        bool enable;           // nazwa programu
        uint8_t modbusAddres;           // opis programu
    };

    class GeneralSettings{
    public:
        GeneralSettings() : softwareName_Cfg("ConfigSoftware"), softwareDesc_Cfg("Software for DKD company"), versionFirmware_Cfg("0.0.0.1"), lang_Cfg("en"),
            namePortsCom("COM1") {}

        QString softwareName_Cfg;           // nazwa programu
        QString softwareDesc_Cfg;           // opis programu
        QString versionFirmware_Cfg;        // wersja firmware
        QString lang_Cfg;                   // "en", "pl", ...

        //ustawienia dresu modbus urzadzenia
        uint8_t addresDevice; //adres urzadzenia modbus

        //ustwienia portu com
        QString namePortsCom;   //nazwa portu com

        SettingsDevice settingsPortCom;    //ustawienia port COM
    };



    void setValue(GeneralSettings settings);
    //====================================================================
    GeneralSettings generalSettings;



//signals:

//public slots:


};

#endif // CONFIGSOFTWARE_H
