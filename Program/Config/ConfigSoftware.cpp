#include "ConfigSoftware.h"

#include <QSettings>
#include <QFile>
#include <QDebug>

//ConfigSoftware::ConfigSoftware(QObject *parent) : QObject(parent)
ConfigSoftware::ConfigSoftware()
{



}

ConfigSoftware::~ConfigSoftware()
{



}


/**
 * Odczyt konfiguracji z pliku
 * @param filePath Pelna sciezka do pliku
 * @return enum ze statusem operacji
*/
int ConfigSoftware::loadFromFile(QString filePath)
{
    if(! QFile::exists(filePath))
        return ConfigSoftware::NO_FILE;

    QSettings settings(filePath, QSettings::IniFormat);
    int status = settings.status();
    if(status == QSettings::FormatError )
        return ConfigSoftware::FILE_CORRUPTED;

    // naglowek
    settings.beginGroup("head");
   if( !settings.contains("CFG_VER_MAJOR") || !settings.contains("CFG_VER_MAINOR"))
        return ConfigSoftware::FILE_CORRUPTED;
    int cfgMajorVer = settings.value("CFG_VER_MAJOR").toInt();
    int cfgMinorVer = settings.value("CFG_VER_MAINOR").toInt();
    // sprawdzenie wersji
    qDebug() << "Readed cfg file ver: " << cfgMajorVer << "." << cfgMinorVer;
    settings.endGroup();
    if(cfgMajorVer != CFG_VER_MAJOR)
        return ConfigSoftware::FILE_INVALID_VER;

    int missedParameters = 0;

    //ogolne
     settings.beginGroup("firmware");
    if(settings.contains("softwareName_Cfg")) {
        generalSettings.softwareName_Cfg = settings.value("softwareName_Cfg").toString();
    } else {missedParameters++;}

    if(settings.contains("softwareDesc_Cfg")) {
        generalSettings.softwareDesc_Cfg = settings.value("softwareDesc_Cfg").toString();
    } else {missedParameters++;}

    if(settings.contains("versionFirmware_Cfg")) {
        generalSettings.versionFirmware_Cfg = settings.value("versionFirmware_Cfg").toString();
    } else {missedParameters++;}

    if(settings.contains("lang_Cfg")) {
        generalSettings.lang_Cfg = settings.value("lang_Cfg").toBool();
    } else {missedParameters++;}
    settings.endGroup();

    //konfiguracja modbus urzadzenia
    settings.beginGroup("modbusConfig");
    if(settings.contains("addresDevice")) {
        generalSettings.addresDevice = settings.value("addresDevice").toInt();
    } else {missedParameters++;}

    //konfiguracja portu com
    settings.beginGroup("portConfig");

    if(settings.contains("namePortsCom")) {
        generalSettings.namePortsCom = settings.value("namePortsCom").toString();
    } else {missedParameters++;}

    if(settings.contains("baudCom")) {
        generalSettings.settingsPortCom.baud = settings.value("baudCom").toInt();
    } else {missedParameters++;}

    if(settings.contains("dataBitsCom")) {
        generalSettings.settingsPortCom.dataBits = settings.value("dataBitsCom").toInt();
    } else {missedParameters++;}

    if(settings.contains("parityCom")) {
        generalSettings.settingsPortCom.parity= settings.value("parityCom").toInt();
    } else {missedParameters++;}

    if(settings.contains("stopBitsCom")) {
        generalSettings.settingsPortCom.stopBits = settings.value("stopBitsCom").toInt();
    } else {missedParameters++;}

    if(settings.contains("responseTimeDevice")) {
        generalSettings.settingsPortCom.responseTime = settings.value("responseTimeDevice").toInt();
    } else {missedParameters++;}

    if(settings.contains("numberOfRetriesDevice")) {
        generalSettings.settingsPortCom.numberOfRetries = settings.value("numberOfRetriesDevice").toInt();
    } else {missedParameters++;}

    settings.endGroup();

   if(missedParameters > 0)
        qDebug() << "!!!!!!!!!!! Config file missed parameters: " << missedParameters << " !!!!!!!!!!!!";

    return ConfigSoftware::OK;
}

/**
 * Zapis konfiguracji do pliku
 * @param filePath Pelna sciezka gdzie zapisac plik
 * @return enum ze statusem operacji
*/
int ConfigSoftware::save2File(QString filePath)
{
    QSettings settings(filePath, QSettings::IniFormat);

    if(settings.isWritable()){

        // naglowek
        settings.beginGroup("head");
        settings.setValue("CFG_VER_MAJOR", CFG_VER_MAJOR);
        settings.setValue("CFG_VER_MAINOR", CFG_VER_MAINOR);
        settings.endGroup();

        //ogolne
        settings.beginGroup("firmware");
        settings.setValue("softwareName_Cfg",           generalSettings.softwareName_Cfg);
        settings.setValue("softwareDesc_Cfg",          generalSettings.softwareDesc_Cfg);
        settings.setValue("versionFirmware_Cfg",          generalSettings.versionFirmware_Cfg);
        settings.setValue("lang_Cfg",               generalSettings.lang_Cfg);
        settings.endGroup();

        //konfiguracja modbus urzadzenia
        settings.beginGroup("modbusConfig");
        settings.setValue("addresDevice",      generalSettings.addresDevice);
        settings.endGroup();


        //konfiguracja portu com
        settings.beginGroup("portConfig");
        settings.setValue("namePortsCom",      generalSettings.namePortsCom);

        settings.setValue("baudCom",      generalSettings.settingsPortCom.baud);
        settings.setValue("dataBitsCom",      generalSettings.settingsPortCom.dataBits);
        settings.setValue("parityCom",      generalSettings.settingsPortCom.parity);
        settings.setValue("stopBitsCom",      generalSettings.settingsPortCom.stopBits);

        settings.setValue("responseTimeCom",      generalSettings.settingsPortCom.responseTime);
        settings.setValue("numberOfRetriesCom",      generalSettings.settingsPortCom.numberOfRetries);

       settings.endGroup();

        settings.sync();
        return ConfigSoftware::OK;
    }

    return ConfigSoftware::UNABLE_2_WRITE;
}


///** Utworzenie danych defaults */
void ConfigSoftware::createDefaultDataSets(void)
{






}

void ConfigSoftware::setValue(GeneralSettings settings)
{
    generalSettings.addresDevice = settings.addresDevice;

    generalSettings.lang_Cfg = settings.lang_Cfg;
    generalSettings.namePortsCom = settings.namePortsCom;

    generalSettings.settingsPortCom = settings.settingsPortCom;

    generalSettings.softwareDesc_Cfg = settings.softwareDesc_Cfg;
    generalSettings.softwareName_Cfg = settings.softwareName_Cfg;
    generalSettings.versionFirmware_Cfg = settings.versionFirmware_Cfg;
}

