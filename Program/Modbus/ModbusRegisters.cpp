#include "ModbusRegisters.h"

ModbusRegisters::ModbusRegisters(int quantityBlockReg, int sizeBlockReg)
{
    m_holdingRegisters.resize(quantityBlockReg);

    for(int i=0;i<m_holdingRegisters.length();i++)
    {
        m_holdingRegisters[i].resize(sizeBlockReg);
    }


//    QVector< QVector< int > > twoDArray;      // Empty.
//    QVector< QVector< int > > twoDArray( 2 ); // Contains two int arrays.
//    twoDArray[0].resize(4);
//    twoDArray[0][2] = 4;  // Assign to the third element of the first array.
//    ...
//    etc...
}



