#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include <QMap>
#include <QSettings>
#include <QToolButton>
#include <QProgressDialog>
#include <QMessageBox>

#include "GUI/Widgets/ConsoleWidget.h"
#include "inc/def.h"



namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    enum EPageIndexes{
        PAGE_IDX_WELCOME,
        PAGE_IDX_CONFIGURATION_MD04,
        PAGE_IDX_CALIBRATION_AP01,
        PAGE_IDX_TEST_CM03
    };

public slots:
    void changePage(EPageIndexes pageIdx);
    void enableCM03Page();

private slots:


    void on_actionExit_triggered();
    void on_consoleButton_toggled(bool checked);

    void showMsgBox(QString msg, QString title = "", int icon = QMessageBox::Information);
    void addConsoleMessage(QString msg, int icon = QMessageBox::NoIcon);

    void on_deviceButton_clicked();

    void on_configurationButton_clicked();
    void on_calibrationButton_clicked();
    void on_ManualButton_clicked();

   void on_actionSetup_triggered();
   void on_actionConnect_triggered();
   void on_actionDisconnect_triggered();



   void on_testDeviceButton_clicked();

private:

    Ui::MainWindow *ui;

    QSettings* m_settings;
    QString m_menuItemEnableStyle;
    QString m_menuItemDisableStyle;
    QString m_menuItemSelectedStyle;
    QString m_filePath;
    bool m_newfileConfiguration = false;


    QList<QToolButton *> m_leftToolbarButtonsList;
    QMap<QString, avaibleViews_t> m_menuButtonName2viewIdx; // [nazwa przycisku w lewym menu] => indeks widoku

    QMap<EPageIndexes, int> m_pageIdx2StackWidgetIdx; // odwzorowanie indeks strony na indeks widgetu wew. StackWidget

    ConsoleWidget* m_console = nullptr;


};

#endif // MAINWINDOW_H
